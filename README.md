# e3-nblmapp  

This module controls data acquisition from ADC3110 for the neutron Beam Loss Monitor system.

## Requirements

- `hdf5-devel`
- `libtiff`
- `blosc`
- `netcdf`
- `tcl`, `tcl-devel`, `tclx` (possibly only two of these)

## EPICS dependencies

```sh
$ make dep
ADSUPPORT_DEP_VERSION = 1.9.0
ASYN_DEP_VERSION = 4.41.0
NDS3EPICS_DEP_VERSION = 1.0.1
NDS3_DEP_VERSION = 3.0.0
TSC_DEP_VERSION = 4.0.8
require nblmapp,1.0.0+0
< configured ...
```

## Installation

```sh
$ make build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r nblmapp
```

In CSS the main opi to run is `nBLM_daq.opi`.

## Contributing

Contributions through pull/merge requests only.
