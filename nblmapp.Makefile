
## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



REQUIRED += asyn nds3epics adsupport nds3 tsc

APPSRC := src
APPDB := db

USR_INCLUDES += -I$(where_am_I)$(APPSRC)
USR_INCLUDES += -I$(where_am_I)$(APPSRC)/include
USR_CXXFLAGS += -std=c++1y -DIS_EEE_MODULE -DIS_EEE_MODULE_NO_TRACE -fpermissive

SOURCES += $(wildcard $(APPSRC)/*.cpp)
SOURCES += $(wildcard $(APPSRC)/*.c)

DBDS += $(APPSRC)/nblmapp_asub.dbd
DBDS += $(APPSRC)/IFC14AIChannelGroup.dbd


SUBS = $(wildcard $(APPDB)/*.substitutions)
TEMPLATES += $(wildcard $(APPDB)/*.db)
SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)


.PHONY: vlibs
vlibs:
