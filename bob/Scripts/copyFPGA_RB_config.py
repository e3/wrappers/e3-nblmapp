# quick copy the readback config into the output config PVs
# file format:
# <pv_value11> <pv_name1>
# <pv_value12> <pv_name2>
#   ...

import os       # check if file exists
import errno
import sys      # shell arg
import time     # get date for file name
import epics    # epics channel acces (caget(), caput())

from save_and_restore_like_user_config import * # user config: PV list

# import my epics functions
sys.path.insert(0, server_nblmapp_PATH)
from epicsFunctions import * # secure caget and caput (blocant function with read back)

def main():
    print("start FPGA RB config copy...")

    PREFIX_ARG= PREFIX
    DEVICE_ARG = DEVICE
    # check arg and write over default values
    # print 'sys.argv: ', sys.argv
    if len(sys.argv) == 2:
        PREFIX_ARG    = sys.argv[1]
    elif len(sys.argv) == 3:
        PREFIX_ARG    = sys.argv[1]
        DEVICE_ARG    = sys.argv[2]

    for j in range(6):
        for i in range(len(PVlistTosaveAndRestore)):
            # pv name to write into
            pvName = PREFIX_ARG + ':' + DEVICE_ARG + ':' + CHANNEL[j] + PVlistTosaveAndRestore[i]
            # pv name RB
            pvNameRB = pvName + '-RB'
            # connect PV-RB
            myPV_RB = connectPV(pvNameRB)
            # get value from PV
            pvValue = myCaget(myPV_RB)

            # connect to PV to write into
            myPV = connectPV(pvName)
            # push data to PVs
            myCaput(myPV, pvValue, myPV_RB)
            print(pvName + ' copied')
        print("")

    # Global parameters
    for i in range(len(PVgloballistToCopy)):
        # pv name to write into
        pvName = PREFIX_ARG + ':' + DEVICE_ARG + ':' + PVgloballistToCopy[i]
        # pv name RB
        pvNameRB = pvName + '-RB'
        # connect PV-RB
        myPV_RB = connectPV(pvNameRB)
        # get value from PV
        pvValue = myCaget(myPV_RB)

        # connect to PV to write into
        myPV = connectPV(pvName)
        # push data to PVs
        myCaput(myPV, pvValue, myPV_RB)
        print(pvName + ' copied')

    print("...FPGA RB config copied")

if __name__ == "__main__":
    main()
