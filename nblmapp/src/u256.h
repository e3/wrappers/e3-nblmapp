/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 */
#ifndef __U256_H__
#define __U256_H__
#include <stdint.h>

struct u256
{
  uint64_t d[4];		// 3 -MSW, 0 - LSW
  unsigned bits;
};


#ifdef __cplusplus
extern "C" {
#endif

void u256_shift_in (struct u256 *d, uint8_t n[16]);
uint64_t u256_get_bitfield (struct u256 *d, uint8_t maxindex, uint8_t minindex);
uint64_t u256_get_top_bits_and_cut (struct u256 *d, int bits);

#ifdef __cplusplus
}
#endif

#endif /* __U256_H__ */