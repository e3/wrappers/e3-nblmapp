/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by the European Spallation Source ERIC
 * Author: Joao Paulo Martins 
 * 		   ESS, Lund, Sweden
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <nblmdrv.h>
#include <cstring>
#include <unistd.h>

#include "IFC14.h"
#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"
#include "evdet.h"


IFC14AIChannel::IFC14AIChannel(const std::string& name, nds::Node& parentNode, int32_t channelNum, ifcdaqdrv_usr_t & deviceUser) :
    //maxOfRawDataInCSS(SCOPE_RAW_DATA_SAMPLES_MAX),
    //maxOfEventInCSS(SCOPE_RAW_DATA_SAMPLES_MAX),
    //maxOfNeutronInCSS(SCOPE_RAW_DATA_SAMPLES_MAX),
    sampleRawDataIdx(0),
    sampleEventIdx(0),
    sampleNeutronIdx(0),
    DODpreview(false),
    save_Q_TOT_single_neutron(0),
    save_current_trigger_period(0),
    m_AM_Channel(channelNum),
    m_deviceUser(deviceUser),
    // Neutron count PVs (from CB6) (scalar data every Monitoring Time Window MTW=1 µs)
    MTW_n(nds::PVVariableIn<std::vector<int32_t> >("MTW_n-RB")),
    MTW_q_n(nds::PVVariableIn<std::vector<int32_t> >("MTW_q_n-RB")),
    MTW_negSat(nds::PVVariableIn<std::vector<int32_t> >("MTW_negSat-RB")),
    MTW_posSat(nds::PVVariableIn<std::vector<int32_t> >("MTW_posSat-RB")),
    MTW_Q_bckgnd(nds::PVVariableIn<std::vector<int32_t> >("MTW_Q_bckgnd-RB")),
    MTW_Q_total(nds::PVVariableIn<std::vector<int32_t> >("MTW_Q_total-RB")),
    
    // Event info PVs from CB"channel" [0-5]
    MTWindx(nds::PVVariableIn<std::vector<double> >("MTWindx-RB")),
    Q_TOT(nds::PVVariableIn<std::vector<int32_t> >("Q_TOT-RB")),
    TOT(nds::PVVariableIn<std::vector<int32_t> >("TOT-RB")),
    TOTstartTime(nds::PVVariableIn<std::vector<int32_t> >("TOTstartTime-RB")),
    peakTime(nds::PVVariableIn<std::vector<int32_t> >("peakTime-RB")),
    peakValue(nds::PVVariableIn<std::vector<int32_t> >("peakValue-RB")),
    serialNumber(nds::PVVariableIn<std::vector<int32_t> >("serialNumber-RB")),
    
    TOTlimit(nds::PVVariableIn<std::vector<int8_t> >("TOTlimit-RB")),
    TOTvalid(nds::PVVariableIn<std::vector<int8_t> >("TOTvalid-RB")),
    isPart2(nds::PVVariableIn<std::vector<int8_t> >("isPart2-RB")),
    peakValid(nds::PVVariableIn<std::vector<int8_t> >("peakValid-RB")),
    pileUp(nds::PVVariableIn<std::vector<int8_t> >("pileUp-RB")),
    eventValid(nds::PVVariableIn<std::vector<int8_t> >("eventValid-RB")),

    // Raw data sample (max size = SCOPE_RAW_DATA_SAMPLES_MAX) from CB8 (only channel 0 for first version)
    rawData(nds::PVVariableIn<std::vector<int32_t> >("rawData-RB")),
    scalarRwData(nds::PVVariableIn<int32_t>("scalarRwData-RB")),
    xAxisRawData(nds::PVVariableIn<std::vector<int32_t> >("xAxisRawData-RB")),

    // Periodic data from CB7
    countT(nds::PVVariableIn<double>("countT-RB")),
    beamInT(nds::PVVariableIn<int32_t>("beamInT-RB")),
    neutron_avg(nds::PVVariableIn<double>("n_avg-RB")),
    neutron_min(nds::PVVariableIn<double>("n_min-RB")),
    neutron_max(nds::PVVariableIn<double>("n_max-RB")),
    Qbackground_avg(nds::PVVariableIn<double>("QbckGndAvg-RB")),
    Qbackground_min(nds::PVVariableIn<double>("QbckGndMin-RB")),
    Qbackground_max(nds::PVVariableIn<double>("QbckGndMax-RB")),
    QtotalAvg(nds::PVVariableIn<double>("QtotalAvg-RB")),
    QtotalMin(nds::PVVariableIn<double>("QtotalMin-RB")),
    QtotalMax(nds::PVVariableIn<double>("QtotalMax-RB")),
    Qneutron_avg(nds::PVVariableIn<double>("Qn_avg-RB")),
    Qneutron_min(nds::PVVariableIn<double>("Qn_min-RB")),
    Qneutron_max(nds::PVVariableIn<double>("Qn_max-RB")),
    loss_over_beamON(nds::PVVariableIn<double>("lossBeamON-RB")),
    loss_over_pulse(nds::PVVariableIn<double>("lossPulse-RB")),

    Tnom_num_of_samples(nds::PVVariableIn<std::int32_t>("TnomSize-RB")),
    Tnom_sum_of_samples(nds::PVVariableIn<double>("TnomSum-RB")),
    Tnom_sum_of_squares(nds::PVVariableIn<double>("TnomSquares-RB")),
    Tnom_negative_saturations(nds::PVVariableIn<std::int32_t>("TnomNegSat-RB")),
    Tnom_positive_saturations(nds::PVVariableIn<std::int32_t>("TnomPosSat-RB")),
    Tnom_pedestal(nds::PVVariableIn<double>("TnomPdstal-RB")),
    Tnom_rms_noise(nds::PVVariableIn<double>("TnomNoise-RB")),

    lossDecimation_factor(nds::PVVariableIn<double>("lossDecim-RB")),
    lossWindow_position(nds::PVVariableIn<double>("lossWinPos-RB")),
    lossWindow_samples(nds::PVVariableIn<double>("lossWinSize-RB")),
    lossWords(nds::PVVariableIn<double>("lossWords-RB")),
	lossData_avg(nds::PVVariableIn<std::vector<double> >("lossData_avg-RB")),
	lossData_max(nds::PVVariableIn<std::vector<double> >("lossData_max-RB")),

    protectionDecimation_factor(nds::PVVariableIn<double>("protDecim-RB")),
    protectionWindow_position(nds::PVVariableIn<double>("protWinPos-RB")),
    protectionWindow_samples(nds::PVVariableIn<double>("protWinSize-RB")),
    protectionWords(nds::PVVariableIn<double>("protWords-RB")),
	protAvg0Max(nds::PVVariableIn<std::vector<double> >("protAvg0Max-RB")),
	protAvg1Max(nds::PVVariableIn<std::vector<double> >("protAvg1Max-RB")),
	protRelaxMax(nds::PVVariableIn<std::vector<double> >("protRelaxMax-RB")),
	protXY_count(nds::PVVariableIn<std::vector<double> >("protXY_count-RB")),

    Tcurr_single_TOTmin(nds::PVVariableIn<std::int32_t>("s_n_TOTmin-RB")),
    Tcurr_single_TOTmax(nds::PVVariableIn<std::int32_t>("s_n_TOTmax-RB")),
    Tcurr_single_TOTaverage(nds::PVVariableIn<double>("s_n_TOTavg-RB")),
    Tcurr_single_TOTrms(nds::PVVariableIn<double>("s_n_TOTrms-RB")),
    Tcurr_single_peakValueMin(nds::PVVariableIn<std::int32_t>("s_n_PeakMin-RB")),
    Tcurr_single_peakValueMax(nds::PVVariableIn<std::int32_t>("s_n_PeakMax-RB")),
    Tcurr_single_peakValueAverage(nds::PVVariableIn<double>("s_n_PeakAvg-RB")),
    Tcurr_single_peakValueRms(nds::PVVariableIn<double>("s_n_PeakRms-RB")),
    Tcurr_single_Q_TOTmin(nds::PVVariableIn<std::int32_t>("s_n_QTOTmin-RB")),
    Tcurr_single_Q_TOTmax(nds::PVVariableIn<std::int32_t>("s_n_QTOTmax-RB")),
    Tcurr_single_Q_TOTaverage(nds::PVVariableIn<double>("s_n_QTOTavg-RB")),
    Tcurr_single_Q_TOTrms(nds::PVVariableIn<double>("s_n_QTOTrms-RB")),
    Tcurr_single_eventCount(nds::PVVariableIn<std::int32_t>("s_n_EvtCnt-RB")),

    Tcurr_pileUp_TOTmin(nds::PVVariableIn<std::int32_t>("s_PU_TOTmin-RB")),
    Tcurr_pileUp_TOTmax(nds::PVVariableIn<std::int32_t>("s_PU_TOTmax-RB")),
    Tcurr_pileUp_TOTaverage(nds::PVVariableIn<double>("s_PU_TOTavg-RB")),
    Tcurr_pileUp_TOTrms(nds::PVVariableIn<double>("s_PU_TOTrms-RB")),
    Tcurr_pileUp_peakValueMin(nds::PVVariableIn<std::int32_t>("s_PU_PeakMin-RB")),
    Tcurr_pileUp_peakValueMax(nds::PVVariableIn<std::int32_t>("s_PU_PeakMax-RB")),
    Tcurr_pileUp_peakValueAverage(nds::PVVariableIn<double>("s_PU_PeakAvg-RB")),
    Tcurr_pileUp_peakValueRms(nds::PVVariableIn<double>("s_PU_PkRms-RB")),
    Tcurr_pileUp_Q_TOTmin(nds::PVVariableIn<std::int32_t>("s_PU_QTOTmin-RB")),
    Tcurr_pileUp_Q_TOTmax(nds::PVVariableIn<std::int32_t>("s_PU_QTOTmax-RB")),
    Tcurr_pileUp_Q_TOTaverage(nds::PVVariableIn<double>("s_PU_QTOTavg-RB")),
    Tcurr_pileUp_Q_TOTrms(nds::PVVariableIn<double>("s_PU_QTOTrms-RB")),
    Tcurr_pileUp_eventCount(nds::PVVariableIn<std::int32_t>("s_PU_EvtCnt-RB")),

    Tcurr_allEvt_TOTmin(nds::PVVariableIn<std::int32_t>("s_AE_TOTmin-RB")),
    Tcurr_allEvt_TOTmax(nds::PVVariableIn<std::int32_t>("s_AE__TOTmax-RB")),
    Tcurr_allEvt_TOTaverage(nds::PVVariableIn<double>("s_AE_TOTavg-RB")),
    Tcurr_allEvt_TOTrms(nds::PVVariableIn<double>("s_AE_TOTrms-RB")),
    Tcurr_allEvt_peakValueMin(nds::PVVariableIn<std::int32_t>("s_AE_PeakMin-RB")),
    Tcurr_allEvt_peakValueMax(nds::PVVariableIn<std::int32_t>("s_AE_PeakMax-RB")),
    Tcurr_allEvt_peakValueAverage(nds::PVVariableIn<double>("s_AE_PeakAvg-RB")),
    Tcurr_allEvt_peakValueRms(nds::PVVariableIn<double>("s_AE_PeakRms-RB")),
    Tcurr_allEvt_Q_TOTmin(nds::PVVariableIn<std::int32_t>("s_AE_QTOTmin-RB")),
    Tcurr_allEvt_Q_TOTmax(nds::PVVariableIn<std::int32_t>("s_AE_QTOTmax-RB")),
    Tcurr_allEvt_Q_TOTaverage(nds::PVVariableIn<double>("s_AE_QTOTavg-RB")),
    Tcurr_allEvt_Q_TOTrms(nds::PVVariableIn<double>("s_AE_QTOTrms-RB")),
    Tcurr_allEvt_eventCount(nds::PVVariableIn<std::int32_t>("s_AE__EvtCnt-RB")),

    Tcurr_bckgnd_TOTmin(nds::PVVariableIn<std::int32_t>("s_BG_TOTmin-RB")),
    Tcurr_bckgnd_TOTmax(nds::PVVariableIn<std::int32_t>("s_BG_TOTmax-RB")),
    Tcurr_bckgnd_TOTaverage(nds::PVVariableIn<double>("s_BG_TOTavg-RB")),
    Tcurr_bckgnd_TOTrms(nds::PVVariableIn<double>("s_BG_TOTrms-RB")),
    Tcurr_bckgnd_peakValueMin(nds::PVVariableIn<std::int32_t>("s_BG_PeakMin-RB")),
    Tcurr_bckgnd_peakValueMax(nds::PVVariableIn<std::int32_t>("s_BG_PeakMax-RB")),
    Tcurr_bckgnd_peakValueAverage(nds::PVVariableIn<double>("s_BG_PeakAvg-RB")),
    Tcurr_bckgnd_peakValueRms(nds::PVVariableIn<double>("s_BG_PeakRms-RB")),
    Tcurr_bckgnd_Q_TOTmin(nds::PVVariableIn<std::int32_t>("s_BG_QTOTmin-RB")),
    Tcurr_bckgnd_Q_TOTmax(nds::PVVariableIn<std::int32_t>("s_BG_QTOTmax-RB")),
    Tcurr_bckgnd_Q_TOTaverage(nds::PVVariableIn<double>("s_BG_QTOTavg-RB")),
    Tcurr_bckgnd_Q_TOTrms(nds::PVVariableIn<double>("s_BG_QTOTrms-RB")),
    Tcurr_bckgnd_eventCount(nds::PVVariableIn<std::int32_t>("s_BG_EvtCnt-RB")),


    // READBACK OF PVs WHICH ARE ALSO DEFINED IN OUTPUT 
    evtThr(nds::PVVariableIn<std::int32_t>("evtThr-RB")),
    evtThr2(nds::PVVariableIn<std::int32_t>("evtThr2-RB")),
    QTOT_n_set(nds::PVVariableIn<double>("QTOT_n_set-RB")),
    n_AmpMin(nds::PVVariableIn<std::int32_t>("n_AmpMin-RB")),
    n_TOTMinIdx(nds::PVVariableIn<std::int32_t>("n_TOTMinIdx-RB")),
    pedestal(nds::PVVariableIn<std::int32_t>("pedestal-RB")),
    PU_TOTStart(nds::PVVariableIn<std::int32_t>("PU_TOTStart-RB")),
    channelSrc(nds::PVVariableIn<std::int32_t>("channelSrc-RB")),
    pdstlNoEvt(nds::PVVariableIn<std::int32_t>("pdstlNoEvt-RB")),
    pdstlWStart(nds::PVVariableIn<std::int32_t>("pdstlWStart-RB")),
    pdstlWSize(nds::PVVariableIn<std::int32_t>("pdstlWSize-RB")),
    WStartLoss(nds::PVVariableIn<std::int32_t>("WStartLoss-RB")),
    WSizeLoss(nds::PVVariableIn<std::int32_t>("WSizeLoss-RB")),
    nomTrigger(nds::PVVariableIn<double>("nomTrigger-RB")),
    currentTriggerPeriod(nds::PVVariableIn<double>("currentTriggerPeriod-RB")),
    n_Cnt(nds::PVVariableIn<std::int32_t>("n_Cnt-RB")),
    filtr0Size(nds::PVVariableIn<std::int32_t>("filtr0Size-RB")),
    filtr1Size(nds::PVVariableIn<std::int32_t>("filtr1Size-RB")),
    xyThr(nds::PVVariableIn<double>("xyThr-RB")),
    ma0Thr(nds::PVVariableIn<double>("ma0Thr-RB")),
    ma1Thr(nds::PVVariableIn<double>("ma1Thr-RB")),
    avPulseThr(nds::PVVariableIn<double>("avPulseThr-RB")),
    expThr(nds::PVVariableIn<double>("expThr-RB")),
    filterxThr(nds::PVVariableIn<std::int32_t>("filterxThr-RB")),
    filterSize(nds::PVVariableIn<std::int32_t>("filterSize-RB")),
    lambda(nds::PVVariableIn<double>("lambda-RB")),
    beamPrmitLUT(nds::PVVariableIn<double>("beamPrmitLUT-RB")),
    WWaveStart(nds::PVVariableIn<std::int32_t>("WWaveStart-RB")),
    WWaveSize(nds::PVVariableIn<std::int32_t>("WWaveSize-RB")),
    WWaveDecim(nds::PVVariableIn<std::int32_t>("WWaveDecim-RB")),
    data_channel(nds::PVVariableIn<std::int32_t>("dataChannel-RB"))
//WARNING NO COMMA IN THE PREVIOUS LAST LINE
{
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT1-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT2-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT3-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT4-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT5-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT6-RB"));
    lossOverT.push_back(nds::PVVariableIn<double>("lossOverT7-RB"));

    m_node = parentNode.addChild(nds::Node(name));

    readNonPeriodicCBchannel.mutexRetrieveData = PTHREAD_MUTEX_INITIALIZER;
    readNonPeriodicCBchannel.condRetrieveData = PTHREAD_COND_INITIALIZER;
    readNonPeriodicCBchannel.retievePause = false;

    // Neutron count CB6 PVs (scalar data every 1 µs)
    MTW_n.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_n.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_n);
    MTW_q_n.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_q_n.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_q_n);
    MTW_negSat.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_negSat.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_negSat);
    MTW_posSat.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_posSat.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_posSat);
    MTW_Q_bckgnd.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_Q_bckgnd.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_Q_bckgnd);
    MTW_Q_total.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTW_Q_total.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTW_Q_total);    
    
    // Event info PVs
    MTWindx.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    MTWindx.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(MTWindx);
    Q_TOT.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    Q_TOT.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Q_TOT);
    TOT.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    TOT.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(TOT);
    TOTlimit.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    TOTlimit.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(TOTlimit);
    TOTstartTime.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    TOTstartTime.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(TOTstartTime);
    TOTvalid.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    TOTvalid.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(TOTvalid);
    isPart2.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    isPart2.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(isPart2);
    peakTime.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    peakTime.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(peakTime);
    peakValid.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    peakValid.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(peakValid);
    peakValue.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    peakValue.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(peakValue);
    pileUp.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    pileUp.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(pileUp);
    serialNumber.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    serialNumber.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(serialNumber);
    eventValid.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    eventValid.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(eventValid);

    // Raw data sample (max size = SCOPE_RAW_DATA_SAMPLES_MAX)
    rawData.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    rawData.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(rawData);

    scalarRwData.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(scalarRwData);

    xAxisRawData.setMaxElements(SCOPE_RAW_DATA_SAMPLES_MAX);
    xAxisRawData.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(xAxisRawData);

    // Periodic data from CB7
    countT.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(countT);
    beamInT.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(beamInT);
    neutron_avg.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(neutron_avg);
    neutron_min.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(neutron_min);
    neutron_max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(neutron_max);
    Qbackground_avg.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qbackground_avg);
    Qbackground_min.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qbackground_min);
    Qbackground_max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qbackground_max);
    QtotalAvg.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(QtotalAvg);
    QtotalMin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(QtotalMin);
    QtotalMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(QtotalMax);
    Qneutron_avg.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qneutron_avg);
    Qneutron_min.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qneutron_min);
    Qneutron_max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Qneutron_max);
    loss_over_beamON.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(loss_over_beamON);
    loss_over_pulse.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(loss_over_pulse);

    lossDecimation_factor.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossDecimation_factor);
    lossWindow_position.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossWindow_position);
    lossWindow_samples.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossWindow_samples);
    lossWords.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossWords);
	lossData_avg.setMaxElements(1000);
    lossData_avg.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossData_avg);
	lossData_max.setMaxElements(1000);
    lossData_max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(lossData_max);

    protectionDecimation_factor.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protectionDecimation_factor);
    protectionWindow_position.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protectionWindow_position);
    protectionWindow_samples.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protectionWindow_samples);
    protectionWords.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protectionWords);
	protAvg0Max.setMaxElements(1000);
    protAvg0Max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protAvg0Max);
	protAvg1Max.setMaxElements(1000);
    protAvg1Max.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protAvg1Max);
	protRelaxMax.setMaxElements(1000);
    protRelaxMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protRelaxMax);
	protXY_count.setMaxElements(1000);
    protXY_count.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(protXY_count);

    Tcurr_single_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_TOTmin);
    Tcurr_single_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_TOTmax);
    Tcurr_single_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_TOTaverage);
    Tcurr_single_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_TOTrms);
    Tcurr_single_peakValueMin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_peakValueMin);
    Tcurr_single_peakValueMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_peakValueMax);
    Tcurr_single_peakValueAverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_peakValueAverage);
    Tcurr_single_peakValueRms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_peakValueRms);
    Tcurr_single_Q_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_Q_TOTmin);
    Tcurr_single_Q_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_Q_TOTmax);
    Tcurr_single_Q_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_Q_TOTaverage);
    Tcurr_single_Q_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_Q_TOTrms);
    Tcurr_single_eventCount.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_single_eventCount);

    Tcurr_pileUp_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_TOTmin);
    Tcurr_pileUp_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_TOTmax);
    Tcurr_pileUp_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_TOTaverage);
    Tcurr_pileUp_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_TOTrms);
    Tcurr_pileUp_peakValueMin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_peakValueMin);
    Tcurr_pileUp_peakValueMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_peakValueMax);
    Tcurr_pileUp_peakValueAverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_peakValueAverage);
    Tcurr_pileUp_peakValueRms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_peakValueRms);
    Tcurr_pileUp_Q_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_Q_TOTmin);
    Tcurr_pileUp_Q_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_Q_TOTmax);
    Tcurr_pileUp_Q_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_Q_TOTaverage);
    Tcurr_pileUp_Q_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_Q_TOTrms);
    Tcurr_pileUp_eventCount.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_pileUp_eventCount);

    Tcurr_allEvt_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_TOTmin);
    Tcurr_allEvt_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_TOTmax);
    Tcurr_allEvt_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_TOTaverage);
    Tcurr_allEvt_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_TOTrms);
    Tcurr_allEvt_peakValueMin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_peakValueMin);
    Tcurr_allEvt_peakValueMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_peakValueMax);
    Tcurr_allEvt_peakValueAverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_peakValueAverage);
    Tcurr_allEvt_peakValueRms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_peakValueRms);
    Tcurr_allEvt_Q_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_Q_TOTmin);
    Tcurr_allEvt_Q_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_Q_TOTmax);
    Tcurr_allEvt_Q_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_Q_TOTaverage);
    Tcurr_allEvt_Q_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_Q_TOTrms);
    Tcurr_allEvt_eventCount.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_allEvt_eventCount);

    Tcurr_bckgnd_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_TOTmin);
    Tcurr_bckgnd_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_TOTmax);
    Tcurr_bckgnd_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_TOTaverage);
    Tcurr_bckgnd_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_TOTrms);
    Tcurr_bckgnd_peakValueMin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_peakValueMin);
    Tcurr_bckgnd_peakValueMax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_peakValueMax);
    Tcurr_bckgnd_peakValueAverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_peakValueAverage);
    Tcurr_bckgnd_peakValueRms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_peakValueRms);
    Tcurr_bckgnd_Q_TOTmin.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_Q_TOTmin);
    Tcurr_bckgnd_Q_TOTmax.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_Q_TOTmax);
    Tcurr_bckgnd_Q_TOTaverage.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_Q_TOTaverage);
    Tcurr_bckgnd_Q_TOTrms.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_Q_TOTrms);
    Tcurr_bckgnd_eventCount.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tcurr_bckgnd_eventCount);
    for(uint8_t i=0; i < LOSS_PERIOD_NB; i++)
    {
        lossOverT.at(i).setScanType(nds::scanType_t::interrupt);
        m_node.addChild(lossOverT.at(i));
    }
   


    Tnom_num_of_samples.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_num_of_samples);
    Tnom_sum_of_samples.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_sum_of_samples);
    Tnom_sum_of_squares.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_sum_of_squares);
    Tnom_negative_saturations.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_negative_saturations);
    Tnom_positive_saturations.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_positive_saturations);
    Tnom_pedestal.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_pedestal);
    Tnom_rms_noise.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(Tnom_rms_noise);


    // READBACK OF PVs WHICH ARE ALSO DEFINED IN OUTPUT
    evtThr.setScanType(nds::scanType_t::interrupt);
    evtThr2.setScanType(nds::scanType_t::interrupt);
    QTOT_n_set.setScanType(nds::scanType_t::interrupt);
    n_AmpMin.setScanType(nds::scanType_t::interrupt);
    n_TOTMinIdx.setScanType(nds::scanType_t::interrupt);
    pedestal.setScanType(nds::scanType_t::interrupt);
    PU_TOTStart.setScanType(nds::scanType_t::interrupt);
    channelSrc.setScanType(nds::scanType_t::interrupt);
    pdstlNoEvt.setScanType(nds::scanType_t::interrupt);
    pdstlWStart.setScanType(nds::scanType_t::interrupt);
    pdstlWSize.setScanType(nds::scanType_t::interrupt);
    WStartLoss.setScanType(nds::scanType_t::interrupt);
    WSizeLoss.setScanType(nds::scanType_t::interrupt);
    nomTrigger.setScanType(nds::scanType_t::interrupt);
    currentTriggerPeriod.setScanType(nds::scanType_t::interrupt);
    n_Cnt.setScanType(nds::scanType_t::interrupt);
    filtr0Size.setScanType(nds::scanType_t::interrupt);
    filtr1Size.setScanType(nds::scanType_t::interrupt);
    xyThr.setScanType(nds::scanType_t::interrupt);
    ma0Thr.setScanType(nds::scanType_t::interrupt);
    ma1Thr.setScanType(nds::scanType_t::interrupt);
    avPulseThr.setScanType(nds::scanType_t::interrupt);
    expThr.setScanType(nds::scanType_t::interrupt);
    filterxThr.setScanType(nds::scanType_t::interrupt);
    filterSize.setScanType(nds::scanType_t::interrupt);
    lambda.setScanType(nds::scanType_t::interrupt);
    beamPrmitLUT.setScanType(nds::scanType_t::interrupt);
    WWaveStart.setScanType(nds::scanType_t::interrupt);
    WWaveSize.setScanType(nds::scanType_t::interrupt);
    WWaveDecim.setScanType(nds::scanType_t::interrupt);
    data_channel.setScanType(nds::scanType_t::interrupt);

    m_node.addChild(evtThr);
    m_node.addChild(evtThr2);
    m_node.addChild(QTOT_n_set);
    m_node.addChild(n_AmpMin);
    m_node.addChild(n_TOTMinIdx);
    m_node.addChild(pedestal);
    m_node.addChild(PU_TOTStart);
    m_node.addChild(channelSrc);
    m_node.addChild(pdstlNoEvt);
    m_node.addChild(pdstlWStart);
    m_node.addChild(pdstlWSize);
    m_node.addChild(WStartLoss);
    m_node.addChild(WSizeLoss);
    m_node.addChild(nomTrigger);
    m_node.addChild(currentTriggerPeriod);
    m_node.addChild(n_Cnt);
    m_node.addChild(filtr0Size);
    m_node.addChild(filtr1Size);
    m_node.addChild(xyThr);
    m_node.addChild(ma0Thr);
    m_node.addChild(ma1Thr);
    m_node.addChild(avPulseThr);
    m_node.addChild(expThr);
    m_node.addChild(filterxThr);
    m_node.addChild(filterSize);
    m_node.addChild(lambda);
    m_node.addChild(beamPrmitLUT);
    m_node.addChild(WWaveStart);
    m_node.addChild(WWaveSize);
    m_node.addChild(WWaveDecim);
    m_node.addChild(data_channel);

    // OUTPUT PVs
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("evtThr", std::bind(&IFC14AIChannel::set_eventDetection_thr, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("evtThr2", std::bind(&IFC14AIChannel::set_eventDetection_thr2, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("QTOT_n_set", std::bind(&IFC14AIChannel::set_inv_of_Q_TOT_single_neutron, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("n_AmpMin", std::bind(&IFC14AIChannel::set_neutronAmpl_min, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("n_TOTMinIdx", std::bind(&IFC14AIChannel::set_neutronTOT_min_indx, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("pedestal", std::bind(&IFC14AIChannel::set_pedestal, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("PU_TOTStart", std::bind(&IFC14AIChannel::set_pileupTOT_start_indx, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("channelSrc", std::bind(&IFC14AIChannel::set_channel_src_select, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("pdstlNoEvt", std::bind(&IFC14AIChannel::set_pedestalExcludeEvents, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("pdstlWStart", std::bind(&IFC14AIChannel::set_pedestal_window_start, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("pdstlWSize", std::bind(&IFC14AIChannel::set_pedestal_window_length, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("WStartLoss", std::bind(&IFC14AIChannel::set_window_start_loss, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("WSizeLoss", std::bind(&IFC14AIChannel::set_window_length_loss, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("nomTrigger", std::bind(&IFC14AIChannel::set_nominal_trigger_period, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("currentTriggerPeriod", std::bind(&IFC14AIChannel::set_current_trigger_period, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("n_Cnt", std::bind(&IFC14AIChannel::set_single_neutron_count, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("filtr0Size", std::bind(&IFC14AIChannel::set_filter0_length, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("filtr1Size", std::bind(&IFC14AIChannel::set_filter1_length, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("xyThr", std::bind(&IFC14AIChannel::set_x_y_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("ma0Thr", std::bind(&IFC14AIChannel::set_ma_0_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("ma1Thr", std::bind(&IFC14AIChannel::set_ma_1_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("avPulseThr", std::bind(&IFC14AIChannel::set_av_pulse_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("expThr", std::bind(&IFC14AIChannel::set_exp_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("filterxThr", std::bind(&IFC14AIChannel::set_filter_x_threshold, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("filterSize", std::bind(&IFC14AIChannel::set_filter_length, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("lambda", std::bind(&IFC14AIChannel::set_lambda, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("beamPrmitLUT", std::bind(&IFC14AIChannel::set_beam_permit_LUT, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("WWaveStart", std::bind(&IFC14AIChannel::set_window_params_wave_start, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("WWaveSize", std::bind(&IFC14AIChannel::set_window_params_wave_length, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("WWaveDecim", std::bind(&IFC14AIChannel::set_window_params_wave_decimation, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("softTrig", std::bind(&IFC14AIChannel::set_softTrigLevel, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT1", std::bind(&IFC14AIChannel::clr_LossOverT1, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT2", std::bind(&IFC14AIChannel::clr_LossOverT2, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT3", std::bind(&IFC14AIChannel::clr_LossOverT3, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT4", std::bind(&IFC14AIChannel::clr_LossOverT4, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT5", std::bind(&IFC14AIChannel::clr_LossOverT5, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT6", std::bind(&IFC14AIChannel::clr_LossOverT6, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("clrT7", std::bind(&IFC14AIChannel::clr_LossOverT7, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("subPdtl", std::bind(&IFC14AIChannel::substractPedestal, this, std::placeholders::_1, std::placeholders::_2)));

    // Create one HDF5 object by IFC
    hdf5Obj = std::make_shared<hdf5_for_one_ifc>(m_deviceUser, m_AM_Channel);
}

void IFC14AIChannel::set_eventDetection_thr(const timespec &timespec, const int32_t &value){
    //convert int32_t into int17_t
    uint32_t regVal = (uint32_t)value & 0x0001FFFF;
    
    printf("[Slot%d][CH%d] %s : write input val = %d, reg= %u\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, regVal);
    write_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD, m_AM_Channel, regVal, m_deviceUser);
    get_eventDetection_thr(); // Read-back value to check
}

void IFC14AIChannel::set_eventDetection_thr2(const timespec &timespec, const int32_t &value){
    //convert int32_t into int17_t
    uint32_t regVal = (uint32_t)value & 0x0001FFFF;

    printf("[Slot%d][CH%d] %s : write input val = %d, reg= %u\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, regVal);
    write_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD2, m_AM_Channel, value, m_deviceUser);
    get_eventDetection_thr2(); // Read-back value to check
}

void IFC14AIChannel::set_inv_of_Q_TOT_single_neutron(const timespec &timespec, const double &value){
    save_Q_TOT_single_neutron = value;
    if(value != 0)
    {
        printf("set %d in INVERSE_OF_QTOT_SINGLE_NEUTRON register for channel %d\n", (uint32_t)((-100.0/value)*(1ull<<32)), m_AM_Channel);
        write_am_banked_reg (RS_INVERSE_OF_QTOT_SINGLE_NEUTRON, m_AM_Channel, (uint32_t)((-100.0/value)*(1ull<<32)), m_deviceUser);
    }
    get_inv_of_Q_TOT_single_neutron(); // Read-back value to check
}

void IFC14AIChannel::set_neutronAmpl_min(const timespec &timespec, const int32_t &value){
    //convert int32_t into int17_t
    uint32_t regVal = (uint32_t)value & 0x0001FFFF;

    printf("[Slot%d][CH%d] %s : write input val = %d, reg= %u\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, regVal);
    write_am_banked_reg (RS_NEUTRON_AMPL_MIN, m_AM_Channel, value, m_deviceUser);
    get_neutronAmpl_min(); // Read-back value to check
}

void IFC14AIChannel::set_neutronTOT_min_indx(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_NEUTRON_TOT_MIN, m_AM_Channel, (uint32_t)(value & 0x0000FFFF), m_deviceUser);
    get_neutronTOT_min_indx(); // Read-back value to check
}

void IFC14AIChannel::set_pedestal(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_PEDESTAL, m_AM_Channel, (uint32_t)(value & 0x0000FFFF), m_deviceUser);
    get_pedestal(); // Read-back value to check
}

void IFC14AIChannel::set_pileupTOT_start_indx(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_PILEUP_TOT_START_INDX, m_AM_Channel, (uint32_t)(value & 0x0000FFFF), m_deviceUser);
    get_pileupTOT_start_indx(); // Read-back value to check
}

// Channel data source:
// 0 - ADC channel number = data processing channel number
// 1 - ADC channel number = data processing channel number + 1
// 2 - ADC channel number = data processing channel number + 2
// 3 - reference data from dummy data generator
void IFC14AIChannel::set_channel_src_select(const timespec &timespec, const int32_t &value){
   struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    write_am_banked_reg (RS_CHANNEL_SRC_SELECT, m_AM_Channel, (uint32_t)(value & 0x00000003), m_deviceUser);
    get_channel_src_select(); // Read-back value to check

    // Readback value of the ADC channel selected
    data_channel.push(now, (int32_t)(m_AM_Channel + value));
}

void IFC14AIChannel::set_pedestalExcludeEvents(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_PEDESTAL_EXCLUDE_EVENTS, m_AM_Channel, (uint32_t)(value == 0), m_deviceUser);
    get_pedestalExcludeEvents(); // Read-back value to check
}

void IFC14AIChannel::set_pedestal_window_start(const timespec &timespec, const int32_t &value){
    // should be even
    write_am_banked_reg (RS_PEDESTAL_WINDOW_START, m_AM_Channel, (uint32_t)(value & 0x01FFFFFE), m_deviceUser);
    get_pedestal_window_start(); // Read-back value to check
}

void IFC14AIChannel::set_pedestal_window_length(const timespec &timespec, const int32_t &value){
    // should be even
    write_am_banked_reg (RS_PEDESTAL_WINDOW_LENGTH, m_AM_Channel, (uint32_t)(value & 0x01FFFFFE), m_deviceUser);
    get_pedestal_window_length(); // Read-back value to check
}

void IFC14AIChannel::set_window_start_loss(const timespec &timespec, const int32_t &value){
 uint32_t lengthPart;
 uint32_t startPart;

    startPart = ((uint32_t)value & (uint32_t)((1<<17) -1));
    lengthPart = read_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, m_deviceUser) & (uint32_t)0x07FE0000;
    write_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, lengthPart | startPart, m_deviceUser);
    get_window_start_loss(); // Read-back value to check
}

void IFC14AIChannel::set_window_length_loss(const timespec &timespec, const int32_t &value){
 uint32_t lengthPart;
 uint32_t startPart;

    lengthPart = ((uint32_t)value << 17) & (uint32_t)0x07FE0000;
    startPart = read_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, m_deviceUser) & (uint32_t)((1<<17) -1);
    write_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, lengthPart | startPart, m_deviceUser);
    get_window_length_loss(); // Read-back value to check
}


void IFC14AIChannel::set_nominal_trigger_period(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_NOMINAL_TRIGGER_PERIOD, m_AM_Channel, (uint32_t)(value/8), m_deviceUser);
    get_nominal_trigger_period(); // Read-back value to check
}

void IFC14AIChannel::set_current_trigger_period(const timespec &timespec, const double &value){
    save_current_trigger_period = value;
    // Obsolete
    //write_am_banked_reg (RS_CURRENT_TRIGGER_PERIOD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    //get_current_trigger_period(); // Read-back value to check
}

void IFC14AIChannel::set_single_neutron_count(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_SINGLE_NEUTRON_COUNT, m_AM_Channel, (uint32_t)(value & 0x00003FFF), m_deviceUser);
    get_single_neutron_count(); // Read-back value to check
}

void IFC14AIChannel::set_filter0_length(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_FILTER0_LENGTH, m_AM_Channel, (uint32_t)(value & 0x00000007), m_deviceUser);
    get_filter0_length(); // Read-back value to check
}

void IFC14AIChannel::set_filter1_length(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_FILTER1_LENGTH, m_AM_Channel, (uint32_t)(value & 0x0000007F), m_deviceUser);
    get_filter1_length(); // Read-back value to check
}

void IFC14AIChannel::set_x_y_threshold(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_X_Y_THRESHOLD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_x_y_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_ma_0_threshold(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_MA_0_THRESHOLD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_ma_0_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_ma_1_threshold(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_MA_1_THRESHOLD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_ma_1_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_av_pulse_threshold(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_AV_PULSE_THRESHOLD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_av_pulse_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_exp_threshold(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_EXP_THRESHOLD, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_exp_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_filter_x_threshold(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_X_THRESHOLD, m_AM_Channel, (uint32_t)(value & 0x00000007), m_deviceUser);
    get_filter_x_threshold(); // Read-back value to check
}

void IFC14AIChannel::set_filter_length(const timespec &timespec, const int32_t &value){
    write_am_banked_reg (RS_Y, m_AM_Channel, (uint32_t)(value & 0x00000007), m_deviceUser);
    get_filter_length(); // Read-back value to check
}

void IFC14AIChannel::set_lambda(const timespec &timespec, const double &value){
    printf("set %d in RS_LAMBDA register for channel %d\n", (uint32_t)(value*(1ull<<32)), m_AM_Channel);
    write_am_banked_reg (RS_LAMBDA, m_AM_Channel, (uint32_t)(value*(1ull<<32)), m_deviceUser);
    get_lambda(); // Read-back value to check
}

void IFC14AIChannel::set_beam_permit_LUT(const timespec &timespec, const double &value){
    write_am_banked_reg (RS_BEAM_PERMIT_LUT, m_AM_Channel, (uint32_t)value, m_deviceUser);
    get_beam_permit_LUT(); // Read-back value to check
}

void IFC14AIChannel::set_window_params_wave_start(const timespec &timespec, const int32_t &value){
 uint32_t decimationAndLengthParts;
 uint32_t startPart;

    startPart = ((uint32_t)value & (uint32_t)((1<<17) -1));
    decimationAndLengthParts = read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser) & (uint32_t)0x3FFE0000;
    write_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, decimationAndLengthParts | startPart, m_deviceUser);
    get_window_params_wave_start(); // Read-back value to check
}

void IFC14AIChannel::set_window_params_wave_length(const timespec &timespec, const int32_t &value){
 uint32_t decimationAndStartParts;
 uint32_t lengthPart;

    lengthPart = ((uint32_t)value << 17) & 0x07FE0000;
    decimationAndStartParts = read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser) & (uint32_t)0x3801FFFF;
    write_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, decimationAndStartParts | lengthPart, m_deviceUser);
    get_window_params_wave_length(); // Read-back value to check
}

void IFC14AIChannel::set_window_params_wave_decimation(const timespec &timespec, const int32_t &value){
 uint32_t decimationPart;
 uint32_t lengthAndStartParts;

    decimationPart = (uint32_t)(value-1) << 27;
    lengthAndStartParts = read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser) & (uint32_t)0x07FFFFFF;
    write_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, decimationPart | lengthAndStartParts, m_deviceUser);
    get_window_params_wave_decimation(); // Read-back value to check
}

void IFC14AIChannel::set_softTrigLevel(const timespec &timespec, const int32_t &value){
    softTrigLevel = value;
}

void IFC14AIChannel::clr_LossOverT1(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[0] = 0;
    lossOverT.at(0).push(now, accumulatedLosses[0]);
}

void IFC14AIChannel::clr_LossOverT2(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[1] = 0;
    lossOverT.at(1).push(now, accumulatedLosses[1]);
}

void IFC14AIChannel::clr_LossOverT3(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[2] = 0;
    lossOverT.at(2).push(now, accumulatedLosses[2]);
}

void IFC14AIChannel::clr_LossOverT4(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[3] = 0;
    lossOverT.at(3).push(now, accumulatedLosses[3]);
}

void IFC14AIChannel::clr_LossOverT5(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[4] = 0;
    lossOverT.at(4).push(now, accumulatedLosses[4]);
}

void IFC14AIChannel::clr_LossOverT6(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[5] = 0;
    lossOverT.at(5).push(now, accumulatedLosses[5]);
}

void IFC14AIChannel::clr_LossOverT7(const timespec &timespec, const int32_t &value){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    accumulatedLosses[6] = 0;
    lossOverT.at(6).push(now, accumulatedLosses[6]);
}

void IFC14AIChannel::substractPedestal(const timespec &timespec, const int32_t &value){
    if(value)
        substractPdtal = true;
    else
        substractPdtal = false;
}


void IFC14AIChannel::get_eventDetection_thr(void){
    struct  timespec now;
    int32_t value;
    uint32_t regVal;

    clock_gettime(CLOCK_REALTIME, &now);
    
     regVal = read_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD, m_AM_Channel, m_deviceUser);

    //convert int17_t into int32_t
    if(regVal & 0x00010000)
        value = (int32_t)(regVal | 0xFFFF0000);
    else
        value = (int32_t)regVal;

     evtThr.push(now, value);
     printf("[Slot%d][CH%d] %s : read reg= %u, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, regVal, value);
}

void IFC14AIChannel::get_eventDetection_thr2(void){
    struct  timespec now;
    int32_t value;
    uint32_t regVal;

    clock_gettime(CLOCK_REALTIME, &now);

     regVal = read_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD2, m_AM_Channel, m_deviceUser);

    //convert int17_t into int32_t
    if(regVal & 0x00010000)
        value = (int32_t)(regVal | 0xFFFF0000);
    else
        value = (int32_t)regVal;

     evtThr2.push(now, value);
     printf("[Slot%d][CH%d] %s : reg= %u, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, regVal, value);
}

void IFC14AIChannel::get_inv_of_Q_TOT_single_neutron(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_INVERSE_OF_QTOT_SINGLE_NEUTRON, m_AM_Channel, m_deviceUser);
     if(value != 0)
     {
        QTOT_n_set.push(now, (double)((-100.0/value)*(1ull<<32)));
        printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)((-100.0/value)*(1ull<<32)));
     }
}

void IFC14AIChannel::get_neutronAmpl_min(void){
    struct  timespec now;
    int32_t value;
    uint32_t regVal;

    clock_gettime(CLOCK_REALTIME, &now);

     regVal = read_am_banked_reg (RS_NEUTRON_AMPL_MIN, m_AM_Channel, m_deviceUser);

    //convert int17_t into int32_t
    if(regVal & 0x00010000)
        value = (int32_t)(regVal | 0xFFFF0000);
    else
        value = (int32_t)regVal;

     n_AmpMin.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %u, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, regVal, value);
}

void IFC14AIChannel::get_neutronTOT_min_indx(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_NEUTRON_TOT_MIN, m_AM_Channel, m_deviceUser)) & 0x0000FFFF;
     n_TOTMinIdx.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_pedestal(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_PEDESTAL, m_AM_Channel, m_deviceUser)) & 0x0000FFFF;
     pedestalReg = (uint16_t)(value-32767);
     pedestal.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_pileupTOT_start_indx(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_PILEUP_TOT_START_INDX, m_AM_Channel, m_deviceUser)) & 0x0000FFFF;
     PU_TOTStart.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_channel_src_select(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_CHANNEL_SRC_SELECT, m_AM_Channel, m_deviceUser)) & 0x00000003;
     channelSrc.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_pedestalExcludeEvents(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_PEDESTAL_EXCLUDE_EVENTS, m_AM_Channel, m_deviceUser);
     pdstlNoEvt.push(now, (int32_t)(value == 0));
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (int32_t)(value == 0));
}

void IFC14AIChannel::get_pedestal_window_start(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_PEDESTAL_WINDOW_START, m_AM_Channel, m_deviceUser)) & 0x01FFFFFF;
     pdstlWStart.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_pedestal_window_length(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_PEDESTAL_WINDOW_LENGTH, m_AM_Channel, m_deviceUser)) & 0x01FFFFFF;
     pdstlWSize.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_window_start_loss(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, m_deviceUser)) & (uint32_t)((1<<17) -1);
     WStartLoss.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_window_length_loss(void){
    struct  timespec now;
    int32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_WINDOW_PARAMS_LOSS, m_AM_Channel, m_deviceUser) & (uint32_t)0x07FE0000) >> 17;
     WSizeLoss.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_nominal_trigger_period(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_NOMINAL_TRIGGER_PERIOD, m_AM_Channel, m_deviceUser);
     nomTrigger.push(now, (double)value*8);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value*8);
}

void IFC14AIChannel::get_current_trigger_period(void){
    struct  timespec now;
    double  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = save_current_trigger_period; //(double)read_am_banked_reg (RS_CURRENT_TRIGGER_PERIOD, m_AM_Channel, m_deviceUser);
     currentTriggerPeriod.push(now, value);
}

void IFC14AIChannel::get_single_neutron_count(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_SINGLE_NEUTRON_COUNT, m_AM_Channel, m_deviceUser)) & 0x00003FFF;
     n_Cnt.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_filter0_length(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_FILTER0_LENGTH, m_AM_Channel, m_deviceUser)) & 0x00000007;
     filtr0Size.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_filter1_length(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_FILTER1_LENGTH, m_AM_Channel, m_deviceUser)) & 0x0000007F;
     filtr1Size.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_x_y_threshold(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_X_Y_THRESHOLD, m_AM_Channel, m_deviceUser);
     xyThr.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_ma_0_threshold(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_MA_0_THRESHOLD, m_AM_Channel, m_deviceUser);
     ma0Thr.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_ma_1_threshold(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_MA_1_THRESHOLD, m_AM_Channel, m_deviceUser);
     ma1Thr.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_av_pulse_threshold(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_AV_PULSE_THRESHOLD, m_AM_Channel, m_deviceUser);
     avPulseThr.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_exp_threshold(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_EXP_THRESHOLD, m_AM_Channel, m_deviceUser);
     expThr.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_filter_x_threshold(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_X_THRESHOLD, m_AM_Channel, m_deviceUser)) & 0x00000007;
     filterxThr.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_filter_length(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_Y, m_AM_Channel, m_deviceUser)) & 0x00000007;
     filterSize.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_lambda(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_LAMBDA, m_AM_Channel, m_deviceUser);
     lambda.push(now, (double)((double)value/(1ull<<32)));
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)((double)value/(1ull<<32)));
}

void IFC14AIChannel::get_beam_permit_LUT(void){
    struct  timespec now;
    uint32_t  value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_BEAM_PERMIT_LUT, m_AM_Channel, m_deviceUser);
     beamPrmitLUT.push(now, (double)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %f\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (double)value);
}

void IFC14AIChannel::get_window_params_wave_start(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser) & (uint32_t)((1<<17) -1);
     WWaveStart.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_window_params_wave_length(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser) & (uint32_t)0x07FE0000) >> 17;
     WWaveSize.push(now, (int32_t)value);
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, value);
}

void IFC14AIChannel::get_window_params_wave_decimation(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = (read_am_banked_reg (RS_WINDOW_PARAMS_WAVE, m_AM_Channel, m_deviceUser)) >> 27;
     WWaveDecim.push(now, (int32_t)(value+1));
     printf("[Slot%d][CH%d] %s : reg= %d, output val = %d\n", m_deviceUser.card, m_AM_Channel, __PRETTY_FUNCTION__, value, (int32_t)(value+1));
}


void IFC14AIChannel::getInitialConfig(void)
{
    get_eventDetection_thr();
    get_eventDetection_thr2();
    get_inv_of_Q_TOT_single_neutron();
    get_neutronAmpl_min();
    get_neutronTOT_min_indx();
    get_pedestal();
    get_pileupTOT_start_indx();
    get_channel_src_select();
    get_pedestalExcludeEvents();
    get_pedestal_window_start();
    get_pedestal_window_length();
    get_window_start_loss();
    get_window_length_loss();
    get_nominal_trigger_period();
    //obsolete get_current_trigger_period();
    get_single_neutron_count();
    get_filter0_length();
    get_filter1_length();
    get_x_y_threshold();
    get_ma_0_threshold();
    get_ma_1_threshold();
    get_av_pulse_threshold();
    get_exp_threshold();
    get_window_params_wave_start();
    get_window_params_wave_length();
    get_window_params_wave_decimation();
    get_beam_permit_LUT();
    get_filter_length();
    get_filter_x_threshold();
    get_lambda();
}


std::vector<int32_t> IFC14AIChannel::andVector(std::vector<int32_t> myVector1, std::vector<int32_t> myVector2){

    std::vector<int32_t> resultVector;

    if(myVector1.size() != myVector2.size())
    {
        resultVector.push_back(0);
    }
    else
    {
        for (unsigned i=0; i<myVector1.size(); i++)
            resultVector.push_back((int32_t)(myVector1[i] & myVector2[i]));
    }
    
    return resultVector;
}

std::vector<int32_t> IFC14AIChannel::notVector(std::vector<int32_t> myVector){

    std::vector<int32_t> resultVector;

    for (unsigned i=0; i<myVector.size(); i++)
        resultVector.push_back((int32_t)(myVector[i] != 1));
    
    return resultVector;
}


// Data On Demand: Display block of 5000 Neutron counter data
void IFC14AIChannel::readNeutronCounter(struct neutronCounts &data)
{
    struct timespec now;

    if(DODpreview == false)
        sampleNeutronIdx = 0; 

    // Vector Init
    if(sampleNeutronIdx == 0)
    {
        m_int32_NC0.clear();
        m_int32_NC1.clear();
        m_int32_NC2.clear();
        m_int32_NC3.clear();
        m_int32_NC4.clear();
        m_int32_NC5.clear();
    }

    if(DODpreview)
    {    
        if(sampleNeutronIdx < SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            // Copy data to EPICS //
            m_int32_NC0.push_back((int32_t)data.N_n);
            m_int32_NC1.push_back((int32_t)data.N_qtot);
            m_int32_NC2.push_back((int32_t)data.negative_saturations);
            m_int32_NC3.push_back((int32_t)data.positive_saturations);
            m_int32_NC4.push_back(CONVERT_Q<int32_t, int32_t>((int32_t)data.Q_background));
            m_int32_NC5.push_back(CONVERT_Q<int32_t, int32_t>((int32_t)data.Qtotal));

            sampleNeutronIdx++;
        }
        
        if(sampleNeutronIdx >= SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            clock_gettime(CLOCK_REALTIME, &now);
            MTW_n.push(now, m_int32_NC0);
            MTW_q_n.push(now, m_int32_NC1);
            MTW_negSat.push(now, m_int32_NC2);
            MTW_posSat.push(now, m_int32_NC3);
            MTW_Q_bckgnd.push(now, m_int32_NC4);
            MTW_Q_total.push(now, m_int32_NC5);
            
            sampleNeutronIdx = 0;
        }
    }
}

// Data On Demand: Display block of 5000 event detection data
void IFC14AIChannel::readEventDetection(struct eventInfoForArchiving &data)
{
 struct timespec now;

    if(DODpreview == false)
        sampleEventIdx = 0; 

    // Vector Init
    if(sampleEventIdx == 0)
    {
        m_double_ED.clear();
        m_int32_ED0.clear();
        m_int32_ED1.clear();
        m_int32_ED2.clear();
        m_int32_ED3.clear();
        m_int32_ED4.clear();
        m_int32_ED5.clear();
        m_int8_ED0.clear();
        m_int8_ED1.clear();
        m_int8_ED2.clear();
        m_int8_ED3.clear();
        m_int8_ED4.clear();
        m_int8_ED5.clear();
    }
    
    if(DODpreview)
    {
        if(sampleEventIdx < SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            // Copy data to EPICS //
            m_double_ED.push_back(CONVERT_ns<double, double>((double)data.MTWindx));
            m_int32_ED0.push_back(CONVERT_Q<int32_t, int32_t>((int32_t)data.Q_TOT));
            m_int32_ED1.push_back(CONVERT_ns<int32_t, int32_t>((int32_t)data.TOT));
            m_int32_ED2.push_back(CONVERT_ns<int32_t, int32_t>((int32_t)data.TOTstartTime));
            m_int32_ED3.push_back(CONVERT_ns<int32_t, int32_t>((int32_t)data.peakTime));
            m_int32_ED4.push_back(CONVERT_mV<int32_t, int32_t>((int32_t)data.peakValue));
            m_int32_ED5.push_back((int32_t)data.serialNumber);
            
            m_int8_ED0.push_back((int8_t)data.TOTlimitReached);
            m_int8_ED1.push_back((int8_t)data.TOTvalid);
            m_int8_ED2.push_back((int8_t)data.isPart2);
            m_int8_ED3.push_back((int8_t)data.peakValid);
            m_int8_ED4.push_back((int8_t)data.pileUp);
            m_int8_ED5.push_back((int8_t)data.eventValid);

            sampleEventIdx++;
        }
        
        if(sampleEventIdx >= SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            clock_gettime(CLOCK_REALTIME, &now);
            MTWindx.push(now, m_double_ED);
            Q_TOT.push(now, m_int32_ED0);
            TOT.push(now, m_int32_ED1);
            TOTstartTime.push(now, m_int32_ED2);
            peakTime.push(now, m_int32_ED3);
            peakValue.push(now, m_int32_ED4);
            serialNumber.push(now, m_int32_ED5);
            
            TOTlimit.push(now, m_int8_ED0);
            TOTvalid.push(now, m_int8_ED1);
            isPart2.push(now, m_int8_ED2);
            peakValid.push(now, m_int8_ED3);
            pileUp.push(now, m_int8_ED4);
            eventValid.push(now, m_int8_ED5);

            sampleEventIdx = 0;
        }
    }
}

// Data On Demand: Display block of 5000 Raw data
void IFC14AIChannel::scope(double Index0, uint16_t data0,  uint16_t data1)
{
    struct timespec now;

    if(DODpreview == false)
        sampleRawDataIdx = 0;

    // Vector Init
    if(sampleRawDataIdx == 0)
    {
        m_int32_scope.clear();
        // Set axis vecteur data for scope displaying
        m_axis_scope.clear();
    }
    
    if(DODpreview)
    {
        if(sampleRawDataIdx < SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            if(substractPdtal)
            {
                m_int32_scope.push_back(CONVERT_mV<int32_t, uint16_t>(data0-pedestalReg));
                m_int32_scope.push_back(CONVERT_mV<int32_t, uint16_t>(data1-pedestalReg));
            }
            else
            {
                m_int32_scope.push_back(CONVERT_mV<int32_t, uint16_t>(data0));
                m_int32_scope.push_back(CONVERT_mV<int32_t, uint16_t>(data1));
            }

            if( (data0 < softTrigLevel) && (data1 < softTrigLevel) )
                trig = true;

            // When DOD preview is requested, we update the raw data histogram.
            // The raw data histo needs raw data in scalar type.
            // One raw data of the pair is enough for the histo.
            clock_gettime(CLOCK_REALTIME, &now);
            scalarRwData.push(now, (int32_t)data1);

//            m_axis_scope.push_back(CONVERT_ns<double, double>(Index0));
//            m_axis_scope.push_back(CONVERT_ns<double, double>(Index0+1));
            m_axis_scope.push_back(sampleRawDataIdx*4);
            sampleRawDataIdx ++;
            m_axis_scope.push_back(sampleRawDataIdx*4);
            sampleRawDataIdx ++;
        }

        if(sampleRawDataIdx >= SCOPE_RAW_DATA_SAMPLES_MAX)
        {
            clock_gettime(CLOCK_REALTIME, &now);
            rawData.push(now, m_int32_scope);
            xAxisRawData.push(now, m_axis_scope);
            //printf("On envoie %d raw data samples\n", (uint32_t)SCOPE_RAW_DATA_SAMPLES_MAX);

            sampleRawDataIdx = 0;

            if(trig)
            {
                pthread_mutex_lock (&readNonPeriodicCBchannel.mutexRetrieveData);
                readNonPeriodicCBchannel.retievePause = true;
                pthread_cond_broadcast (&readNonPeriodicCBchannel.condRetrieveData);
                pthread_mutex_unlock (&readNonPeriodicCBchannel.mutexRetrieveData);
                trig = false;
            }
        }
    }
}

// CB7: INFO = 1
void IFC14AIChannel::readDetectorSpecificDataInTnom(struct pedestalInfo &pi)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    if(refreshDetectorSpecificDataInTnom >= displayDiscrimination)
    {
        // Copy data to EPICS //
        Tnom_num_of_samples.push(now, (int32_t)pi.num_of_samples);
        Tnom_sum_of_samples.push(now, (double)pi.sum_of_samples);
        Tnom_sum_of_squares.push(now, (double)pi.sum_of_squares);
        Tnom_negative_saturations.push(now, (int32_t)pi.negative_saturations);
        Tnom_positive_saturations.push(now, (int32_t)pi.positive_saturations);
        if(pi.num_of_samples != 0)
        {
            Tnom_pedestal.push(now, (double)((unsigned long long)pi.sum_of_samples / (int32_t)pi.num_of_samples));
            Tnom_rms_noise.push(now, (double)sqrt((double)((unsigned long long)pi.sum_of_squares*(uint32_t)pi.num_of_samples - ((unsigned long long)pi.sum_of_samples*(unsigned long long)pi.sum_of_samples))/(double)((uint32_t)pi.num_of_samples*(uint32_t)pi.num_of_samples)));
        }
        else
        {
            Tnom_pedestal.push(now, (double)0);
            Tnom_rms_noise.push(now, (double)0);
        }
        refreshDetectorSpecificDataInTnom = 0;
    }
    else
    {
        refreshDetectorSpecificDataInTnom++;
    }

}

// CB7: INFO = 8
void IFC14AIChannel::readAccumulatedLossInTnom(struct accumulatedSummary_t &as)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    if(refreshAccumulatedLossInTnom >= displayDiscrimination)
    {
        // Copy data to EPICS //
        beamInT.push(now, (int32_t)as.beam);
        countT.push(now, (double)as.count);
        neutron_avg.push(now,((double)as.neutron_sum/as.count)/100);
        neutron_min.push(now,(double)(as.neutron_min)/100);
        neutron_max.push(now,(double)(as.neutron_max)/100);
        if(as.count != 0)
        {
            Qbackground_avg.push(now,(double)as.Qbackground_sum/as.count);
            QtotalAvg.push(now,(double)as.Qtotal_sum/as.count);
            Qneutron_avg.push(now,((double)as.neutron_sum/as.count)*save_Q_TOT_single_neutron/100);
        }
        else
        {
            Qbackground_avg.push(now,(double)0);
            QtotalAvg.push(now,(double)0);
            Qneutron_avg.push(now,(double)0);
        }
        
        Qbackground_min.push(now,(double)as.Qbackground_min);
        Qbackground_max.push(now,(double)as.Qbackground_max); 
        QtotalMin.push(now,(double)as.Qtotal_min);
        QtotalMax.push(now,(double)as.Qtotal_max); 
        Qneutron_min.push(now,(double)as.neutron_min*save_Q_TOT_single_neutron/100);
        Qneutron_max.push(now,(double)as.neutron_max*save_Q_TOT_single_neutron/100);
        loss_over_beamON.push(now,(double)(as.loss_in_window)/100);
        loss_over_pulse.push(now,(double)(as.neutron_sum)/100);
  
        refreshAccumulatedLossInTnom = 0;
    }
    else
    {
        refreshAccumulatedLossInTnom++;
    }

    for(uint8_t i=0; i < LOSS_PERIOD_NB; i++)
        accumulatedLosses[i] += (double)(as.neutron_sum/100);
    // if((m_AM_Channel == 0 ) && accumulatedLosses[6])
    //     printf("accumulatedLosses[6] = %f\n", accumulatedLosses[6]);
}

// CB7: INFO = 2 to 31 (8 to 31 are unused)
void IFC14AIChannel::readLossInUserDefinedWindow(struct lossInUserDefinedWindow_t &lu)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    if(refreshLossInUserDefinedWindow >= displayDiscrimination)
    {
        // Copy data to EPICS //
        lossDecimation_factor.push(now,(double)(lu.lossDecimation_factor)+1);
        lossWindow_position.push(now,(double)lu.lossWindow_position);
        lossWindow_samples.push(now,(double)lu.lossWindow_samples);
        lossWords.push(now,(double)lu.lossWords);
        lossData_avg.push(now,lu.lossData_avg);
        lossData_max.push(now,lu.lossData_max);
        refreshLossInUserDefinedWindow = 0;
    }
    else
    {
        refreshLossInUserDefinedWindow++;
    }
    
}

// CB7: INFO = 33 to 38
void IFC14AIChannel::readProtectionFunctionOutputs(struct protectionFunctionOutputs_t &po)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    if(refreshProtectionFunctionOutputs >= displayDiscrimination)
    {
        // Copy data to EPICS //
        protectionDecimation_factor.push(now,(double)po.protectionDecimation_factor);
        protectionWindow_position.push(now,(double)po.protectionWindow_position);
        protectionWindow_samples.push(now,(double)po.protectionWindow_samples);
        protectionWords.push(now,(double)po.protectionWords);
        protAvg0Max.push(now,po.protectionAvg0_max);
        protAvg1Max.push(now,po.protectionAvg1_max);
        protRelaxMax.push(now,po.protectionRelaxation_max);
        protXY_count.push(now,po.protectionXY_count);
        refreshProtectionFunctionOutputs = 0;
    }
    else
    {
        refreshProtectionFunctionOutputs++;
    }
}

// CB7: INFO = 9 to 32
void IFC14AIChannel::readEventStatisticsInTcurr(struct eventStatistics_t &es, int type)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    if(refreshEventStatisticsInTcurr >= displayDiscrimination)
    {
        switch(type)
        {
            case 0: // single neutron events
                Tcurr_single_TOTmin.push(now, (int32_t)es.TOTmin);
                Tcurr_single_TOTmax.push(now, (int32_t)es.TOTmax);
                Tcurr_single_TOTaverage.push(now, (double)es.TOTaverage);
                Tcurr_single_TOTrms.push(now, (double)es.TOTrms);
                Tcurr_single_peakValueMin.push(now, (int32_t)es.peakValueMin);
                Tcurr_single_peakValueMax.push(now, (int32_t)es.peakValueMax);
                Tcurr_single_peakValueAverage.push(now, (double)es.peakValueAverage);
                Tcurr_single_peakValueRms.push(now, (double)es.peakValueRms);
                Tcurr_single_Q_TOTmin.push(now, (int32_t)es.Q_TOTmin);
                Tcurr_single_Q_TOTmax.push(now, (int32_t)es.Q_TOTmax);
                Tcurr_single_Q_TOTaverage.push(now, (double)es.Q_TOTaverage);
                Tcurr_single_Q_TOTrms.push(now, (double)es.Q_TOTrms);
                Tcurr_single_eventCount.push(now, (int32_t)es.eventCount);
            break;

    // Obsolete : only case 0 : event // Virer les données des cas 1, 2 et 3
            case 1: // pile-up events
                Tcurr_pileUp_TOTmin.push(now, (int32_t)es.TOTmin);
                Tcurr_pileUp_TOTmax.push(now, (int32_t)es.TOTmax);
                Tcurr_pileUp_TOTaverage.push(now, (double)es.TOTaverage);
                Tcurr_pileUp_TOTrms.push(now, (double)es.TOTrms);
                Tcurr_pileUp_peakValueMin.push(now, (int32_t)es.peakValueMin);
                Tcurr_pileUp_peakValueMax.push(now, (int32_t)es.peakValueMax);
                Tcurr_pileUp_peakValueAverage.push(now, (double)es.peakValueAverage);
                Tcurr_pileUp_peakValueRms.push(now, (double)es.peakValueRms);
                Tcurr_pileUp_Q_TOTmin.push(now, (int32_t)es.Q_TOTmin);
                Tcurr_pileUp_Q_TOTmax.push(now, (int32_t)es.Q_TOTmax);
                Tcurr_pileUp_Q_TOTaverage.push(now, (double)es.Q_TOTaverage);
                Tcurr_pileUp_Q_TOTrms.push(now, (double)es.Q_TOTrms);
                Tcurr_pileUp_eventCount.push(now, (int32_t)es.eventCount);
            break;

            case 2: // all events
                Tcurr_allEvt_TOTmin.push(now, (int32_t)es.TOTmin);
                Tcurr_allEvt_TOTmax.push(now, (int32_t)es.TOTmax);
                Tcurr_allEvt_TOTaverage.push(now, (double)es.TOTaverage);
                Tcurr_allEvt_TOTrms.push(now, (double)es.TOTrms);
                Tcurr_allEvt_peakValueMin.push(now, (int32_t)es.peakValueMin);
                Tcurr_allEvt_peakValueMax.push(now, (int32_t)es.peakValueMax);
                Tcurr_allEvt_peakValueAverage.push(now, (double)es.peakValueAverage);
                Tcurr_allEvt_peakValueRms.push(now, (double)es.peakValueRms);
                Tcurr_allEvt_Q_TOTmin.push(now, (int32_t)es.Q_TOTmin);
                Tcurr_allEvt_Q_TOTmax.push(now, (int32_t)es.Q_TOTmax);
                Tcurr_allEvt_Q_TOTaverage.push(now, (double)es.Q_TOTaverage);
                Tcurr_allEvt_Q_TOTrms.push(now, (double)es.Q_TOTrms);
                Tcurr_allEvt_eventCount.push(now, (int32_t)es.eventCount);
            break;

            case 3: // background events
                Tcurr_bckgnd_TOTmin.push(now, (int32_t)es.TOTmin);
                Tcurr_bckgnd_TOTmax.push(now, (int32_t)es.TOTmax);
                Tcurr_bckgnd_TOTaverage.push(now, (double)es.TOTaverage);
                Tcurr_bckgnd_TOTrms.push(now, (double)es.TOTrms);
                Tcurr_bckgnd_peakValueMin.push(now, (int32_t)es.peakValueMin);
                Tcurr_bckgnd_peakValueMax.push(now, (int32_t)es.peakValueMax);
                Tcurr_bckgnd_peakValueAverage.push(now, (double)es.peakValueAverage);
                Tcurr_bckgnd_peakValueRms.push(now, (double)es.peakValueRms);
                Tcurr_bckgnd_Q_TOTmin.push(now, (int32_t)es.Q_TOTmin);
                Tcurr_bckgnd_Q_TOTmax.push(now, (int32_t)es.Q_TOTmax);
                Tcurr_bckgnd_Q_TOTaverage.push(now, (double)es.Q_TOTaverage);
                Tcurr_bckgnd_Q_TOTrms.push(now, (double)es.Q_TOTrms);
                Tcurr_bckgnd_eventCount.push(now, (int32_t)es.eventCount);
            break;

            default:
            break;
        }
        refreshEventStatisticsInTcurr = 0;
    }
    else
    {
        refreshEventStatisticsInTcurr++;
    }
}
