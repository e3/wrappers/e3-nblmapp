/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by the European Spallation Source ERIC
 * Author: Joao Paulo Martins 
 * 		   ESS, Lund, Sweden
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

#include <nds3/nds.h>
#include <nblmdrv.h>

#include "IFC14.h"
#include "IFC14Device.h"
#include "IFC14AIChannelGroup.h"
#ifdef ENABLE_DATA_CHECK
#include "algo_prepare_reference.h"
#endif
#include "crc32.h"

volatile sig_atomic_t exit_loop = 0;
ifcdaqdrv_usr_t deviceUserTab[NB_MAX_OF_IFC];

void handler(int)
{
 exit_loop = 1;
 usleep(500000); // wait 1/2 sec
 finalize_all_devices();
 signal(SIGINT, SIG_DFL);
}

IFC14Devices::IFC14Devices(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters) :
        m_node(deviceName) {

	crc32_init ();
#ifdef ENABLE_DATA_CHECK
// prepare reference algorithm results. Do it only once.
	prepare_reference ();
#endif

    // Call example in cmd : ndsCreateDevice(nblm, "PBI-nBLM00", chGrp="PBI-AMC-"", grpNb="130_150", MB_DOD="64")
    char cardString[NB_MAX_OF_IFC][4] {'\0'};
    std::string cards = std::string(parameters.at("grpNb"));
    sscanf (cards.c_str(),"%[^_]_%[^_]_%[^_]_%[^_]_%[^_]_%[^_]_%[^_]_%[^_]_%[^_]_%[^_]",cardString[0],cardString[1],cardString[2],cardString[3],cardString[4],cardString[5],cardString[6],cardString[7],cardString[8],cardString[9]);
    
    // DOD Size in cmd is given in MBytes (same size for each IFC1410)
    size_t memSize = static_cast<size_t>(std::stoul(parameters.at("MB_DOD")));

    uint8_t i=0;
    while((cardString[i][0] != '\0') && (i < NB_MAX_OF_IFC))
    {
        ifcdaqdrv_usr_t deviceUser = {0};
        std::string channelGroup = std::string(parameters.at("chGrp")) + cardString[i];

        deviceUser.card = (uint32_t)((uint32_t)atoi(&cardString[i][1]))/10;
        deviceUser.hdf5Name = deviceName + "-" + channelGroup;
        deviceUser.DOD_MemSize =  memSize*1024*1024;
        deviceUserTab[i] = deviceUser;

        // PCIe slot of the IFC1410 board
        std::cout << "push device PCIe slot nb " << deviceUserTab[i].card << " with goup name " << channelGroup << std::endl;
        std::shared_ptr<IFC14AIChannelGroup> aichgrp = std::make_shared<IFC14AIChannelGroup>(channelGroup, m_node, deviceUserTab[i]);
        m_AIChannelGroups.push_back(aichgrp);
        i++;
    }

    signal(SIGHUP,  handler);
    signal(SIGINT,  handler);
    signal(SIGQUIT, handler);
    signal(SIGABRT, handler);
    signal(SIGFPE,  handler);
    signal(SIGSEGV, handler);
    signal(SIGTERM, handler);

    m_node.initialize(this, factory);
}

IFC14Devices::~IFC14Devices() {
    finalize_all_devices();
}

NDS_DEFINE_DRIVER(nblm, IFC14Devices);

