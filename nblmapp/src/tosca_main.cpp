/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <pthread.h>
#include <inttypes.h>
#include <signal.h>
#include <vector>
#include <thread>

extern "C" {
#include "tsculib.h"
#include "tscioctl.h"
};

#include "u256.h"

#include "data_source.h"
#include "evdet.h"
#include "buffers.h"


#ifdef USE_HDF5
#include "hdf5_interface.h"
#endif

#include "interleaver_thread.h"
#include "IFC14AIChannelGroup.h"

//extern struct tsc_ioctl_dma_req dma_req[2];

// volatile sig_atomic_t exit_loop = 0;

// void handler(int)
// {
//  exit_loop = 1;
//  signal(SIGINT, SIG_DFL);
// }

bool data_check = false;

int interrupt_enable = 1;
int debug = 0;

void IFC14AIChannelGroup::main_loop(int t)
{
	struct timespec timeout;
  int result;
  double starttime = rTime ();

  int counter = 0;
  unsigned long size = 0;
  unsigned long long total_size = 0;
  int tsc_fd = m_deviceUser.device.tsc_fd;

  //size = sizeof (buffer);

  printf("Starting main_loop(%d) task\n", t);

  struct tsc_ioctl_user_irq user_irq = {0};
  user_irq.wait_mode = IRQ_WAIT_INTR | IRQ_WAIT_1S | (5 << 4); // Timeout after 2s
  user_irq.irq = t;
  user_irq.mask = 1 << t;

  if(interrupt_enable)
  {
	  printf("DMA bank %d IRQ: Subscribe to user IRQ %d\n", t, user_irq.irq);
    tsc_user_irq_subscribe(tsc_fd, &user_irq);
  }

  for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)
    size_g[current_buffer[t]][ch] = 0;

  while (!exit_loop && !exit_processData_loop)
  {
    counter++;
    if (counter == printout_decimator)
      counter = 0;

    if (interrupt_enable)
    {
      // Enable interrupts for data ready in FPGA DDR
      write_reg (t?REG_CB_INTERRUPT_ENABLE_2:REG_CB_INTERRUPT_ENABLE_1, 1, m_deviceUser);

      int ret = tsc_user_irq_wait(tsc_fd, &user_irq);
      if (ret)
      {
        printf("USER IRQ %d (dma%d irq) timeout\n", user_irq.irq, t);
      }
      else
      {
//    			printf("USER IRQ %d received\n", user_irq.irq);
      }

      if (debug)
      {
        double t = rTime ();
        printf ("Interrupt - timestamp = %.3f ms\n", (t - starttime) / 1000);
      }
    }

    // disable interrupts
    write_reg (t?REG_CB_INTERRUPT_ENABLE_2:REG_CB_INTERRUPT_ENABLE_1, 0, m_deviceUser);

    tsc_user_irq_subscribe(tsc_fd, &user_irq);

    uint32_t data_collected = read_reg (t?REG_CB_DATA_COLLECTED_2:REG_CB_DATA_COLLECTED_1, m_deviceUser) & channels_in_thread[t];
    uint32_t data_overflow = read_reg (REG_CB_DATA_OVERFLOW, m_deviceUser) & channels_in_thread[t];
    uint32_t data_overwritten = read_reg (REG_CB_DATA_OVERWRITTEN, m_deviceUser) & channels_in_thread[t];

    // clear overflow and disable interrupts
    write_reg (REG_CB_CLEAR_OVERFLOW, data_overflow, m_deviceUser);

//      if (data_collected || !counter)
//	printf ("CB_DATA_COLLECTED=%08x\n", data_collected);

    for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)
      if (channels_in_thread[t] & (1 << ch))
      {
        if (data_overflow & (1 << (ch)))
          {
            printf ("Channel %d overflow\n", ch);
          }

        if (data_overwritten & (1 << (ch)))	// overwrite
          {
            // Read the RS_PTR_OVERWRITTEN reg clears the flags in  REG_CB_DATA_OVERWRITTEN reg
            uint32_t overwrite_address =read_banked_reg (RS_PTR_OVERWRITTEN, ch, m_deviceUser);
            if (overwrite_address == read_addr[ch])
            {
              if(ch != 7)
                printf ("Channel %d overwrite - we lost data before DMA transfers\n", ch);
            }
            else
            {
              printf ("Channel %d overwrite - impossible - logic error\n",ch);
            }
          }
      }

    channels_active[t][current_buffer[t]] = data_collected;
    for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)
      if (channels_in_thread[t] & (1 << ch))
      {
        if (!(data_collected & (1 << ch)))
          continue;

      // if some of the channels are active but not read
      // still they must be read periodically to ensure consistency of timestamps
      // read KBUF_SIZE bytes every (15 * 60) / (buffer_size[ch] / KBUF_SIZE) seconds
      // if less bytes are available, read everything
      // this buffer can be overwritten if no smart trigger occured in the meantime
      // latency timer must be also enabled

        int size_r_read = size_r[current_buffer[t]][ch]; //does not need mutex, as both producer and consumer threads use different current_buffer, checked_buffer!
        
        if (size_g[current_buffer[t]][ch] != size_r_read)
          continue;

        uint32_t w_pointer;
        w_pointer = read_banked_reg (RS_W_PTR, ch, m_deviceUser);

    //        if (w_pointer == read_addr[ch])
    //          continue;

    #if 0
          printf ("buffer %d: w_pointer=%08x, read_addr=%08x\n",
            ch, w_pointer, read_addr[ch]);
    #endif

    #if 0 /* page overwrite workaround */
        if (w_pointer > read_addr[ch])
          size = w_pointer - read_addr[ch];
        else
          size = buf_end[ch] - read_addr[ch] + w_pointer - buf_beg[ch];

        if (size > 1*1024*1024)
          size -= 1*1024*1024;
        else
          size = 0;
          
    //	  if (size == 0)
    //	    continue;

        if (read_addr[ch] + size > buf_end[ch])
          size = buf_end[ch] - read_addr[ch];
    #else
        if (w_pointer > read_addr[ch])
          size = w_pointer - read_addr[ch];
        else
          size = buf_end[ch] - read_addr[ch];
    #endif

        if (size > KBUF_SIZE)
          size = KBUF_SIZE;

        //printf("Transfer size: %08x\n", size);

        uint32_t data_overwritten2 =
          read_reg (REG_CB_DATA_OVERWRITTEN, m_deviceUser) & channels_in_thread[t];
        if (data_overwritten2 & (1 << (ch)))	// overwrite
        {
          uint32_t overwrite_address =  read_banked_reg (RS_PTR_OVERWRITTEN, ch, m_deviceUser);
          if (overwrite_address == read_addr[ch])
          {
            if(ch != 7)
              printf ("Channel %d overwrite - we lost data before DMA transfers\n", ch);
          }
          else
          {
            printf ("Channel %d overwrite - impossible - logic error\n", ch);
          }
        }

        if ((size != 0) && transfer_dma (ch, size, t, read_addr[ch], m_deviceUser, current_buffer[t]))
        {
          printf("End of main_loop(%d) task\n", t);
          exit (EXIT_FAILURE);
        }

//        data_read_counter_g[current_buffer[t]][ch] = data_read_counter[ch];
        size_g[current_buffer[t]][ch] = size;
//        data_read_counter[ch] += size;
        read_addr[ch] += size;
        
        if (read_addr[ch] == buf_end[ch])
          read_addr[ch] = buf_beg[ch];
        
        write_banked_reg (RS_R_PTR, ch, read_addr[ch], m_deviceUser);

        data_overwritten2 = read_reg (REG_CB_DATA_OVERWRITTEN, m_deviceUser) & channels_in_thread[t];
        
        if (data_overwritten2 & (1 << (ch)))	// overwrite
        {
          uint32_t overwrite_address =  read_banked_reg (RS_PTR_OVERWRITTEN, ch, m_deviceUser);
          if (overwrite_address == read_addr[ch])
            printf ("Channel %d overwrite - last block valid\n", ch);
          else
            printf ("Channel %d overwrite - last block invalid\n", ch);
            // two subsequent reads cannot have the same read_address
            // we cannot read the entire buffer at once - if read_ptr and write_ptr are at the beginning, it means that buffer is empty
        }

        total_size += size;
      } // end of for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)   +     if (channels_in_thread[t] & (1 << ch))

    // Waiting for the signal from the buffer_checker thread indicating that data has been
    // transfered from the small DDR area to the bigger one
    pthread_mutex_lock (&reader_mutex[t]);
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_sec += 1;
    result = 0;

    while ((checked_buffer[t] != current_buffer[t]) && (!exit_loop) && (!exit_processData_loop))
    {
      result = pthread_cond_timedwait (&condvar[t], &reader_mutex[t], &timeout);
      if (result == ETIMEDOUT)
      {
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 1;
      }
    }
	
    current_buffer[t] = !current_buffer[t];
    pthread_cond_signal (&condvar[t]);
      
    pthread_mutex_unlock (&reader_mutex[t]);
	} // end of while (!exit_loop && !exit_processData_loop)

printf("End of main_loop(%d) task\n", t);
}

int IFC14AIChannelGroup::main_thread (void)
{
  int nominator = 0;
  int denominator = 12;
  int threshold_multiplier = 0; // when 0 is set: each burst generates the interrupt and sets the appropriate DATA_COLLECTED flag
  int latency = 1;
  int input = 0;
  uint32_t channel_mask = channelMask;

	printf("Starting main_thread()) task\n");
  printout_decimator = 1000;

  // FIRST START ?
  if(MainNotStarted)
  {
    if (initialize_driver (m_deviceUser))
      exit (EXIT_FAILURE);

    if(!resetChannels)
      channelMask = get_channel_mask() & ((1 << CB_CHANNEL_NB_MAX) - 1);
    // Retrieve the channel mask previously set in the FPGA
    channel_mask = channelMask;

    printf("First main_thread() task\n");

    for(size_t AM_Channel(0); AM_Channel < AM_CHANNEL_NB_MAX; ++AM_Channel)
    {
        m_AIChannels.at(AM_Channel)->getInitialConfig();
    }
    
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    collectOn.push(now, (int32_t)false);
    get_decimStart();
    get_decimStop();
    get_T1();
    get_T2();
    get_T3();
    get_T4();
    get_T5();
    get_T6();
    get_T7();
    // display versions
    getnBLM_versions();
  }
  else
  {
    printf("\nReconfigure CB channels of the IFC1410 board in PCIe slot %d\n\n", m_deviceUser.card);
  }

  printf("channel mask value = 0x%x\n", channel_mask);

  // disable all channels: stop transmission from FPGA to DDR
  write_reg (REG_CB_DATA_CHANNEL_ENABLE, 0, m_deviceUser);
    while (0 != read_reg (REG_CB_DATA_CHANNEL_ENABLE, m_deviceUser))
      printf ("Waiting for disable REG_CB_DATA_CHANNEL_ENABLE\n");

	// Allocation of big buffer in the DDR CPU
  allocateBuffers();
	// Set read_addr[] needed when setting the FPGA DDR address for CB
  // and buf_beg[], buf_end for RS_BASE_ADDR and RS_END_ADDR setting
  initialize_channel_arrays ();

#ifdef ENABLE_DATA_CHECK
  if(input==3)
    data_check = true;
  else
    data_check = false;
#endif

///////////// start camonitor early
  CircularBufferBlocking<evt_timestamp_pair> cb_evt;
  CircularBufferBlocking<int_timestamp_pair> cb_int;
  CircularBufferBlocking<timestamp_triple> cb_tri[CB_CHANNEL_NB_MAX];
  
  cb_evt.resize(10);
  cb_int.resize(10);
  for(int i=0;i<CB_CHANNEL_NB_MAX;++i)
  {
    cb_tri[i].resize(10);
    //printf("Resize cb_tri[%d]\n", i);
  }
  
  std::thread timestamp_matcher_thread(timestamp_matcher, &cb_int, &cb_evt, &cb_tri[0], channel_mask & (~(uint32_t)(1 << cb_channel_periodic)), std::ref(exit_processData_loop));
  std::thread camonitor_thread(camonitorProcessingThread, &cb_evt, std::ref(exit_processData_loop));
//////////////


  printf ("Using burst size: %d multiplier: %d divisor: %d channels %d\n",
	  burstsize, nominator, denominator, channel_mask);

  write_reg (REG_CB_INTERRUPT_ENABLE_1, 0, m_deviceUser);
  write_reg (REG_CB_INTERRUPT_ENABLE_2, 0, m_deviceUser);

  const int GEN_DIV = denominator;
  const int GEN_MULT = nominator;

  // CB registers initialization : Set reg with Base/End  addresses of each CB in the FPGA DDRs 
  for (uint8_t i = 0; i < CB_CHANNEL_NB_MAX; ++i)
  {
    write_banked_reg (RS_BASE_ADDR, i, buf_beg[i], m_deviceUser);
    write_banked_reg (RS_END_ADDR, i, buf_end[i], m_deviceUser);
    write_banked_reg (RS_BURST_SIZE, i, burstsize, m_deviceUser);
    // Generator data rate multiplier and divider
    write_banked_reg (RS_GEN_MULT_DIV, i, ((GEN_MULT << 24) | (GEN_DIV)), m_deviceUser);
    // The amount of data (in Bytes) which - when collected in DDR3 memory - should generate the interrupt
    // and set the appropriate DATA_COLLECTED flag in CBS register. if DATA_THRESHOLD is set to 0, each burst
    // generates the interrupt and sets the appropriate DATA_COLLECTED flag
    write_banked_reg (RS_DATA_INTERRUPT_THRESHOLD, i,	burstsize * threshold_multiplier, m_deviceUser);
    // The time after which the new transfer should be scheduled
    write_banked_reg (RS_LATENCY_TIMER, i, latency, m_deviceUser);
    // By default the frame contains SAMPLE_THRESHOLD samples
    write_banked_reg (RS_SAMPLE_THRESHOLD, i, 512, m_deviceUser);
  }


  printf("Clk mon 1\n");
  for(int i=0x86; i<=0x8b; ++i)
  {
    uint32_t r = read_reg(i*4, m_deviceUser);
    printf("%" PRIu32 "\n", r);
  } 

// enable ADC clock and DAQ
  write_reg (0x204, 1, m_deviceUser);
  sleep (1);
  write_reg (0x204, 3, m_deviceUser);
  sleep (1);
  write_reg (0x204, 7, m_deviceUser);
  sleep (1);

//  Using backplane trigger line 1 (0 => using internal trigger)
  write_reg(REG_PERIODIC_SOURCE,0x1, m_deviceUser);

  printf("Clk mon 2\n");
  for(int i=0x86; i<=0x8b; ++i)
  {
    uint32_t r = read_reg(i*4, m_deviceUser);
    printf("%" PRIu32 "\n", r);
  } 

//select raw data source
//=> 0x2C688 = 0b101'100'011'010'001'000
//=> data processing ch 0 on CB8, data processing ch 1 on CB9 ... data processing ch 5 on CB13
write_reg (REG_RAW_DATA_SELECTOR, 0x2C688, m_deviceUser);

// set up decimator : Now user configures them...
//write_reg(REG_DECIMATOR_START, 0, m_deviceUser);
//write_reg(REG_DECIMATOR_STOP,  4000000/8, m_deviceUser); //4 ms (delay in ns / 8 ns)

#ifdef DMA_TURBO
// FIRST START ?
  if(MainNotStarted)
    set_turbo_dma_mode(m_deviceUser);
#endif

#ifdef PUT_TIMESTAMP
const uint32_t timestamp_mask = 2;
#else
const uint32_t timestamp_mask = 0;
#endif

// reset ADC clock domain
  write_reg (REG_AM_RESET, 1 | timestamp_mask, m_deviceUser);
  write_reg (REG_AM_ENABLE, 0x3f, m_deviceUser);


  // Set FPGA DDR address for CB
  int ch;
  for (ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)
  {
    write_banked_reg (RS_R_PTR, ch, read_addr[ch], m_deviceUser);	// hw reset does not do that
    // Read the RS_PTR_OVERWRITTEN reg clears the flags in  REG_CB_DATA_OVERWRITTEN reg
    read_banked_reg (RS_PTR_OVERWRITTEN, ch, m_deviceUser);
    // clear overflow
    write_reg (REG_CB_CLEAR_OVERFLOW, 1 << ch, m_deviceUser);
    
    // Reinitialize a global data
    stalled_buffer[ch] = 0;
  }

  // Reinitialize global data 
  current_buffer[0] = 0;
  checked_buffer[0] = 0;
  current_buffer[1] = 0;
  checked_buffer[1] = 0;

  // Reset then enable activated CB
  reset_channels (channel_mask, m_deviceUser);
  write_reg (REG_CB_DATA_CHANNEL_ENABLE, channel_mask, m_deviceUser);
  while (channel_mask != read_reg (REG_CB_DATA_CHANNEL_ENABLE, m_deviceUser))
    printf ("Waiting for enable REG_CB_DATA_CHANNEL_ENABLE\n");

// AM parameters : Now user configures them...
// 	for (i = 0; i < 6; ++i)
//     {
//       write_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD, i, -1638, m_deviceUser);
//       write_am_banked_reg (RS_EVENT_DETECTION_THRESHOLD2, i, -983, m_deviceUser);
//       write_am_banked_reg (RS_INVERSE_OF_QTOT_SINGLE_NEUTRON, i, 2184566, m_deviceUser);
//       write_am_banked_reg (RS_NEUTRON_AMPL_MIN, i, -3604, m_deviceUser);
//       write_am_banked_reg (RS_NEUTRON_TOT_MIN, i, 14, m_deviceUser);
//       write_am_banked_reg (RS_PEDESTAL, i, 32768, m_deviceUser);
//       write_am_banked_reg (RS_PILEUP_TOT_START_INDX, i, 75, m_deviceUser);
//       write_am_banked_reg (RS_CHANNEL_SRC_SELECT, i, input, m_deviceUser);

//       write_am_banked_reg (RS_WINDOW_PARAMS_WAVE, i, (16 << 17) | (1 << 27) , m_deviceUser);

// //      write_am_banked_reg (RS_WINDOW4_PARAMS, i, 0, m_deviceUser);

//       write_am_banked_reg (RS_NOMINAL_TRIGGER_PERIOD, i, 16 * 125*1000, m_deviceUser);
//       write_am_banked_reg (RS_SINGLE_NEUTRON_COUNT, i, 10000, m_deviceUser);
//     }

// release reset to ADC clock domain
  write_reg (REG_AM_RESET, 0 | timestamp_mask, m_deviceUser);


  pthread_cond_init (&condvar[0], NULL);
  pthread_mutex_init (&reader_mutex[0], NULL);
  pthread_cond_init (&condvar[1], NULL);
  pthread_mutex_init (&reader_mutex[1], NULL);
  
  // Run buffer_checker and main_loop tasks if at least one activated CB channel is managed by the task
  if(channels_in_cb[0] & channel_mask)
  {
    consumer_thread0 = m_node.runInThread(std::bind(&IFC14AIChannelGroup::buffer_checker, this, 0));
    dma_thread0 = m_node.runInThread(std::bind(&IFC14AIChannelGroup::main_loop, this, 0));
  }
  if(channels_in_cb[1] & channel_mask)
  {
    consumer_thread1 = m_node.runInThread(std::bind(&IFC14AIChannelGroup::buffer_checker, this, 1));
    dma_thread1 = m_node.runInThread(std::bind(&IFC14AIChannelGroup::main_loop, this, 1));
  }

  // Thread for the timing => IRQ2 wait and process function
  std::thread irq_trigger_thread = std::thread(irq_thread, &cb_int, std::ref(exit_processData_loop), std::ref(m_deviceUser));

  m_epicsInterleaverThread.clear();

  // Run the interleaver tasks for all activated CB (only DOD channels)
  for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ch++)
    if (channel_mask & (1 << ch) & (~(uint32_t)(1 << cb_channel_periodic)))
      m_epicsInterleaverThread.push_back(std::thread(interleaver_thread, std::ref(processors.at(ch)->buffers), std::ref(cb_tri[ch]), std::ref(processors.at(ch)->buffers_timestamped), ch, std::ref(exit_processData_loop), std::ref(readNonPeriodicCB), std::ref(m_deviceUser)));

  // Run the non periodic CB processing task
  if (channel_mask & (~(uint32_t)(1 << cb_channel_periodic)))
    m_epicsNonPeriodicThread = m_node.runInThread(std::bind(&IFC14AIChannelGroup::processNonPeriodicData, this));

  
  m_epicsAccumulatedLossThread.clear();

  if(channel_mask & (uint32_t)(1 << cb_channel_periodic))
  {
    // Run the periodic CB processing task
    m_epicsPeriodicConsumerThread = m_node.runInThread(std::bind(&IFC14AIChannelGroup::processPeriodicData, this));

    // Run 7 tasks for loss calculation over time T1period, T2period, T3period, T4period, T5period, T6period, T7period
    for (uint8_t T = 0; T < LOSS_PERIOD_NB; T++)
      m_epicsAccumulatedLossThread.push_back(m_node.runInThread(std::bind(&IFC14AIChannelGroup::accumulatedLossThread, this, T)));
  }

  MainNotStarted = false;

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Here user asks for CB reconfiguration. exit_processData_loop has been set to 1 => all threads will join //
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(channels_in_cb[0] & channel_mask)
  {
    dma_thread0.join();
    consumer_thread0.join();
  }
  
  if(channels_in_cb[1] & channel_mask)
  {
    dma_thread1.join();
    consumer_thread1.join();
  }
  
  if (channel_mask & (~(uint32_t)(1 << cb_channel_periodic)))
    m_epicsNonPeriodicThread.join();

  if(channel_mask & (uint32_t)(1 << cb_channel_periodic))
  {
    // Wait for end of periodic data retrieving thread
    m_epicsPeriodicConsumerThread.join();
    // Wait for end of periodic counter threads
    for(size_t i=0; i<m_epicsAccumulatedLossThread.size(); i++)
      m_epicsAccumulatedLossThread.at(i).join();
  }

  for(size_t i=0; i<m_epicsInterleaverThread.size(); i++)  
      m_epicsInterleaverThread.at(i).join();

  irq_trigger_thread.join();

  camonitor_thread.join();
  timestamp_matcher_thread.join();

	//Do not free kernel buffer with finalize_driver() as kernel allocation is not so easy...
  freeBuffers();

  printf("End of main_thread() task\n");
  return 0;
}
