/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#include <stdint.h>
#include <math.h>
#include "evdet.h"

void
preprocess (hls::stream<sampleAndTimestamp>& A , hls::stream<preprocessedData>& E, hls::stream<preprocessedInfo>& sample_stream, ap_fixed<17,17> neutronAmpl_min,
		ap_fixed<17,17> eventDetection_thr, ap_fixed<17,17> eventDetection_thr2, uint16_t pedestal)
{
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY max=1
#pragma HLS INTERFACE axis port=A
#pragma HLS INTERFACE axis port=E
#pragma HLS DATA_PACK variable=E
#pragma HLS INTERFACE axis port=sample_stream
#pragma HLS INTERFACE ap_ctrl_none port=return

  uint32_t twodata;
  preprocessedData res;
  sampleAndTimestamp ti;
  A >> ti;
  twodata = ti.sample;

Input_vector_loop:
  for (int i = 0; i < 2; ++i)
    {
	  preprocessedDataItem di;
	  uint16_t data = twodata >> (16 * i);
	  di.sample = data;
	  di.adjusted_sample = ap_fixed<17,17>(data) - ap_fixed<17,17>(pedestal);
	  di.belowThr1 = (di.adjusted_sample <= eventDetection_thr);
	  di.belowThr2 = (di.adjusted_sample <= eventDetection_thr2);
	  di.peakValid = (di.adjusted_sample <= neutronAmpl_min);

	  di.sample_index = ti.sample_index + i;
	  di.frame_index = ti.frame_index;
	  di.newFrame = false;

      if (di.sample_index  == nBinsInMTW - 1)
    	  di.newFrame = true;

	  if(i==0)
	    res.data0 = di;
	  else
		res.data1 = di;
    }

  res.triggers = ti.triggers;
  
  preprocessedInfo pe;
  pe.frame_index = res.data0.frame_index;
  pe.sample_index = res.data0.sample_index;
  pe.sample0 = res.data0.sample;
  pe.sample1 = res.data1.sample;
  E << res;
  sample_stream << pe;
}
