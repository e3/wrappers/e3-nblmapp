/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <stdint.h>
#include "data_source.h"

#ifndef INIT_COUNTER
#define INIT_COUNTER 0
#endif

void datagen(hls::stream<sampleAndTimestamp>& B, uint32_t nominal_period, bool put_timestamp) {
#pragma HLS INTERFACE axis port=B
#pragma HLS PIPELINE II=1
#pragma HLS INTERFACE ap_ctrl_none port=return

const uint16_t data_array[] = {
#include "CB0.h"
};
	static uint32_t counter = INIT_COUNTER;
	static uint8_t sample_index = 0;
	static uint32_t frame_index = 0;
        
        static uint32_t nominal_counter;

	uint32_t data;

if (!put_timestamp)
	data = data_array[counter];
else
	data = (sample_index << 8) | 0xea;

	counter += 1;
	counter = counter % (sizeof(data_array)/sizeof(*data_array));

if (!put_timestamp)
	data |= ((uint32_t)data_array[counter]) << 16;
else
	data = (data << 16) | (frame_index & 0xffff);

	counter += 1;
	counter = counter % (sizeof(data_array)/sizeof(*data_array));

      sampleAndTimestamp ti;
      
        
//      if (trigger_counter != 250'000'000 / 14)
      if (nominal_counter != nominal_period)

          ++nominal_counter;
      else
          nominal_counter = 0;

      if (nominal_counter == 0)
      {
          ti.triggers.pulseTrigger = true;
      }
      else
      {
          ti.triggers.pulseTrigger = false;
      }

      ti.triggers.beamOnPeriodStrobe = true;
      
      ti.sample = data;
      ti.sample_index = sample_index;
      ti.frame_index = frame_index;
      B << ti;


      if (sample_index + 1  == nBinsInMTW - 1)
      {
    	  sample_index = 0;
    	  frame_index++;
      }
      else
    	  sample_index += 2;

}

