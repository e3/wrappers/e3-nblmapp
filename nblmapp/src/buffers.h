/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */

#ifndef __BUFFERS_H__
#define __BUFFERS_H__

#include <csignal>

#include "config.h"
#include "interleaver_thread.h"

const int compute_crc = 1;


//extern uint64_t data_read_counter_g[2][CB_CHANNEL_NB_MAX];
//extern uint64_t data_read_counter[CB_CHANNEL_NB_MAX];


void
write_banked_reg (unsigned reg_selector, unsigned channel,
		  uint32_t value, ifcdaqdrv_usr_t& deviceUser);
void
write_am_banked_reg (unsigned reg_selector, unsigned channel,
		     uint32_t value, ifcdaqdrv_usr_t& deviceUser);
uint32_t
read_am_banked_reg (unsigned reg_selector, unsigned channel, ifcdaqdrv_usr_t& deviceUser);

uint32_t
read_banked_reg (unsigned reg_selector, unsigned channel, ifcdaqdrv_usr_t& deviceUser);

double
rTime ();

#define REG_CB_INTERRUPT_ENABLE_1   0x198
#define REG_CB_DATA_CHANNEL_ENABLE  0x19C
#define REG_CB_DATA_CHANNEL_RESET   0x1A0
#define REG_CB_CLEAR_OVERFLOW       0x1A4
#define REG_CB_DATA_COLLECTED_1     0x1A8
#define REG_CB_FIFO_EMPTY           0x1AC
#define REG_CB_DATA_OVERWRITTEN     0x1B0
#define REG_CB_DATA_OVERFLOW        0x1B4
#define REG_CB_DATA_COLLECTED_2     0x1B8
#define REG_CB_INTERRUPT_ENABLE_2   0x1BC

#define REG_CBRS                    0x1C0
#define REG_CBRV                    0x1C4

#define RS_BASE_ADDR 0
#define RS_END_ADDR 1
#define RS_BURST_SIZE 2
#define RS_DATA_INTERRUPT_THRESHOLD 3
#define RS_LATENCY_TIMER 4
#define RS_R_PTR 5
#define RS_W_PTR 6
#define RS_PTR_OVERWRITTEN 7
#define RS_GEN_MULT_DIV 8
#define RS_SAMPLE_THRESHOLD 9
#define RS_MAX 9

#define REG_AMRS 0x1D0
#define REG_AMRV 0x1D4

#define RS_EVENT_DETECTION_THRESHOLD 0
#define RS_EVENT_DETECTION_THRESHOLD2 1
#define RS_INVERSE_OF_QTOT_SINGLE_NEUTRON 2
#define RS_NEUTRON_AMPL_MIN 3
#define RS_NEUTRON_TOT_MIN 4
#define RS_PEDESTAL 5
#define RS_PILEUP_TOT_START_INDX 6
#define RS_CHANNEL_SRC_SELECT 7
#define RS_PEDESTAL_EXCLUDE_EVENTS 8
#define RS_PEDESTAL_WINDOW_START 9
#define RS_PEDESTAL_WINDOW_LENGTH 10
#define RS_WINDOW_PARAMS_LOSS 11
#define RS_WINDOW_PARAMS_WAVE 12
#define RS_NOMINAL_TRIGGER_PERIOD 13
#define RS_SINGLE_NEUTRON_COUNT 14
#define RS_FILTER0_LENGTH 15
#define RS_FILTER1_LENGTH 16
#define RS_X_Y_THRESHOLD 17
#define RS_MA_0_THRESHOLD 18
#define RS_MA_1_THRESHOLD 19
#define RS_AV_PULSE_THRESHOLD 20
#define RS_EXP_THRESHOLD 21
#define RS_X_THRESHOLD 22
#define RS_Y 23
#define RS_LAMBDA 24
#define RS_BEAM_PERMIT_LUT 25

#define REG_AM_ENABLE 0x1C8
#define REG_AM_RESET 0x1CC

#define REG_TRIGGER_MTW 0x1E0
#define REG_TRIGGER_SAMPLE 0x1E4
#define REG_PERIODIC_SOURCE 0x1E8

#define REG_DECIMATOR_START 0x1EC
#define REG_DECIMATOR_STOP  0x1F0

#define REG_RAW_DATA_SELECTOR		0x1D8
//#define REG_DECIMATOR_PARAMETERS 	0x1DC

// The burst size should be set to 2048 to avoid problems with frame FIFO overflows (Grzegorz)
const int burstsize = 2048  & 0xfffffff0; //256
const uint32_t SOF_WORD_0 = 0x50F50F50u;
const uint32_t SOF_WORD_1 = 0xF50F50F5u;

const uint32_t EOF_WORD_1 = 0xe0fe0fe0u;
const uint32_t EOF_WORD_2 = 0xfe0fe0feu;
const uint32_t EOF_WORD_3 = 0x0fe0fe0fu;

void 	 set_turbo_dma_mode(ifcdaqdrv_usr_t& deviceUser);
void     reset_channels (uint32_t channel_mask, ifcdaqdrv_usr_t& deviceUser);
void     write_reg (unsigned reg_offset, uint32_t value, ifcdaqdrv_usr_t& deviceUser);
uint32_t read_reg (unsigned reg_offset, ifcdaqdrv_usr_t& deviceUser);
int      initialize_driver(ifcdaqdrv_usr_t& deviceUser);
void     finalize_all_devices(void);
void     finalize_driver(ifcdaqdrv_usr_t& deviceUser);
int      transfer_dma(int ch, int size, int t, uint32_t readAddress, ifcdaqdrv_usr_t& deviceUser, int current_buf);
int      wait_for_interrupt();

extern volatile sig_atomic_t exit_loop;

extern bool data_check;
uint64_t interpolate_evt_timestamp(int_timestamp st, timestamp_triple range[2]);
uint64_t int_to_u64(int_timestamp st);
uint64_t evt_to_u64(evt_timestamp st);


#ifdef __x86_64__
#define bswap_32(x) __bswap_32(x)
#else
#define bswap_32(x) (x)
#endif
#endif /* __BUFFERS_H__ */