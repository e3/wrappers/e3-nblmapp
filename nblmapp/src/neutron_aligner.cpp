/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#include <stdint.h>
#include <math.h>
#include "evdet.h"

#include <iostream>
using namespace std;


void
neutron_aligner (hls::stream<eventInfo>& E, hls::stream<eventInfo>& New, hls::stream<eventInfo>& Old)
{
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY min=1 max=1
#pragma HLS INTERFACE axis off port=E
#pragma HLS DATA_PACK variable=E
#pragma HLS INTERFACE axis port=New
#pragma HLS DATA_PACK variable=New
#pragma HLS INTERFACE axis port=Old
#pragma HLS DATA_PACK variable=Old
#pragma HLS INTERFACE ap_ctrl_none port=return

static eventInfo old_event = {0};
eventInfo new_event;
E >> new_event;

New << new_event;
Old << old_event;
if (new_event.eventValid)
	old_event = new_event;
}
