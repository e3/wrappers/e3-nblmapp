/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <inttypes.h>

#include <thread>
#include "buffers.h"
#include <deque>
#include "interleaver_thread.h"

bool match(cpu_timestamp t1, cpu_timestamp t2)
{
  const unsigned udelta=10000; // 10 ms
  uint64_t tt1 = uint64_t(t1.tv_sec)*1000000 + t1.tv_usec;
  uint64_t tt2 = uint64_t(t2.tv_sec)*1000000 + t2.tv_usec;
  return (tt1 < tt2 + udelta) && (tt2 < tt1 + udelta);
}

void timestamp_matcher(CircularBufferBlocking<int_timestamp_pair>* cb_int, CircularBufferBlocking<evt_timestamp_pair>* cb_evt, 
                       CircularBufferBlocking<timestamp_triple>* cb_tri, uint32_t channelmask, volatile bool& exitThread)
#ifdef ASSUME_CONSTANT_TRIGGER_PERIOD
{
evt_timestamp current_timestamp = {10,0};
uint64_t lastts = 0;
uint64_t lastts_evt = 0;

size_t copied_items, size_written;

while((!exit_loop) && (!exitThread))
{
  int_timestamp_pair it;
  copied_items=0;
  cb_int->get_data(&it, 1, 1, copied_items, 1);
  if (copied_items)
  {
      timestamp_triple tt;
      tt.c = it.c;
      tt.e = current_timestamp;
      tt.i = it.i;
      uint64_t newts = tt.c.tv_sec*1000000 + tt.c.tv_usec;
      uint64_t newts_evt = uint64_t(tt.e.seconds)*1000000000 + (uint64_t)tt.e.nanoseconds;

      printf("Matching timestamps (1) found at %ld.%ld" , tt.c.tv_sec,tt.c.tv_usec);
      if (lastts)
         printf(", delta=%" PRIu64 ", delta_evt=%" PRIu64 "\n", newts - lastts, newts_evt - lastts_evt);
      else
        printf("\n");
      lastts=newts;
      lastts_evt=newts_evt;

      for(int j=0;j<CB_CHANNEL_NB_MAX;++j)
        if (channelmask & (1 << j))
        {
          cb_tri[j].put_data(&tt, 1, size_written);
          if(!size_written)
            printf("Channel %d: timestamp lost\n",j);
        }
      
      current_timestamp.nanoseconds += TRIGGER_PERIOD % 1000000000;
      current_timestamp.seconds += TRIGGER_PERIOD / 1000000000;
      
      if (current_timestamp.nanoseconds >= 1000000000)
      {
        current_timestamp.nanoseconds -= 1000000000;
        current_timestamp.seconds += 1;
      }

      //printf("New seconds=%" PRIu32 ", nanoseconds=%" PRIu32 "\n", current_timestamp.seconds, current_timestamp.nanoseconds);

  }
  
  evt_timestamp_pair et;
  cb_evt->get_data(&et, 1, 1, copied_items, 1);
  
}

}
#else
{
std::deque<int_timestamp_pair> int_buffer;
std::deque<evt_timestamp_pair> evt_buffer;
const int maxsize=1;
size_t copied_items, size_written;
//uint64_t lastts = 0;

printf("Starting timestamp_matcher task\n");
while((!exit_loop) && (!exitThread))
{
  int_timestamp_pair it;
  copied_items=0;
  cb_int->get_data(&it, 1, 1, copied_items, 1);
  if (copied_items)
  {
    //printf("it.i.MTW = %d, it.i.sample = %d, it.c.tv_sec = %ld, it.c.tv_usec = %ld\n", it.i.MTW , it.i.sample, it.c.tv_sec, it.c.tv_usec);
    int_buffer.push_front(it);
  }
  if (int_buffer.size()>maxsize)
    int_buffer.pop_back();
  if(copied_items)
    for(auto i: evt_buffer)
    {
      if (match(i.c,it.c))
      {
        timestamp_triple tt;
        tt.c = i.c;
        tt.e = i.e;
        tt.i = it.i;
        //uint64_t newts = tt.c.tv_sec*1000000 + tt.c.tv_usec;
        //YMA
        // printf("Matching timestamps (1) found at %ld.%ld" , tt.c.tv_sec,tt.c.tv_usec);
        // if (lastts)
        //   printf(", delta=%" PRIu64 "\n", newts - lastts);
        // else
        //   printf("\n");
        //lastts=newts;
        for(int j=0;j<CB_CHANNEL_NB_MAX;++j)
          if (channelmask & (1 << j) & (~(uint32_t)(1 << cb_channel_periodic)))
          {
            cb_tri[j].put_data(&tt, 1, size_written);
            //YMA
            // if(!size_written)
            //   printf("Channel %d: timestamp lost\n",j);
          }
      }
    }

  evt_timestamp_pair et;
  //copied_items = 0;
  cb_evt->get_data(&et, 1, 1, copied_items, 1);
  if (copied_items)
  {
    //printf("et.e.seconds = %d, et.e.nanoseconds = %d, et.c.tv_sec = %ld, et.c.tv_usec = %ld\n", et.e.seconds, et.e.nanoseconds, et.c.tv_sec, et.c.tv_usec);
    evt_buffer.push_front(et);
  }
  if (evt_buffer.size()>maxsize)
    evt_buffer.pop_back();
  if(copied_items)
    for(auto i: int_buffer)
    {
      if (match(i.c,et.c))
      {
        timestamp_triple tt;
        tt.c = i.c;
        tt.e = et.e;
        tt.i = i.i;
        // uint64_t newts = tt.c.tv_sec*1000000 + tt.c.tv_usec;
        // printf("Matching timestamps (2) found at %ld.%ld" , tt.c.tv_sec,tt.c.tv_usec);
        // if (lastts)
        //   printf(", delta=%" PRIu64 "\n", newts - lastts);
        // else
        //   printf("\n");
        //lastts=newts;
        for(int j=0;j<CB_CHANNEL_NB_MAX;++j)
          if (channelmask & (1 << j) & (~(uint32_t)(1 << cb_channel_periodic)))
          {
            cb_tri[j].put_data(&tt, 1, size_written);
          }
      }
    }
}
printf("End of timestamp_matcher task\n");
}
#endif
