/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * 
 * Original file created by the CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>

// This create x axis for histogram
static int createXaxis(aSubRecord *precord) {
    double upLimit       = *(double *)precord->a;   // up limit of histogram
    double lowLimit      = *(double *)precord->b;   // low limit of histogram
    short nbOfElement   = *(short *)precord->c;   // number of elemnt of histogram
 
    // printf("upLimit: %hd\n", upLimit);
    // printf("lowLimit: %hd\n", lowLimit);
    // printf("nbOfElement: %hd\n", nbOfElement);
    // width calculated depending on inputs
    float width = (float)(upLimit - lowLimit) / (float)nbOfElement;
    //printf("width calculated: %f\n", width);
    // x axis created
    epicsFloat32 *xAxis = (epicsFloat32 *)precord->vala;  // output axis
    // calculate new x-axis
    int i = 0;
    for(i=0; i<nbOfElement; i++){
        *xAxis++ = lowLimit + i*width;
    }
    // output (histo width)
    *(float *)precord->valb = width;
    return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(createXaxis);
