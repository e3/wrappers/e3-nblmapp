/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by the European Spallation Source ERIC
 * Author: Joao Paulo Martins 
 * 		   ESS, Lund, Sweden
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>
#include <unistd.h>
#include <cstring>
#include <nds3/nds.h>
#include <nblmdrv.h>

#include <epicsExport.h>

#include "IFC14.h"
#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"


int onAMCOne=0;

IFC14AIChannelGroup::IFC14AIChannelGroup(const std::string& name, nds::Node& parentNode, ifcdaqdrv_usr_t & deviceUser) :
    m_node(nds::Port(name, nds::nodeType_t::generic)),
    m_deviceUser(deviceUser),
    m_testCounterPV(nds::PVVariableIn<double>("TstCnter-RB")),
    collectOn(nds::PVVariableIn<std::int32_t>("collect-RB")),
    m_nBLM_infoVersionsPV(nds::PVVariableIn<std::string>("versions-RB")),
    DODpreview(false),
    // READBACK OF PVs WHICH ARE ALSO DEFINED IN OUTPUT
    channel_mask(nds::PVVariableIn<std::int32_t>("channelMask-RB")),
    decimStart(nds::PVVariableIn<double>("decimStart-RB")),
    decimStop(nds::PVVariableIn<double>("decimStop-RB")),
    T1period(nds::PVVariableIn<std::int32_t>("T1period-RB")),
    T2period(nds::PVVariableIn<std::int32_t>("T2period-RB")),
    T3period(nds::PVVariableIn<std::int32_t>("T3period-RB")),
    T4period(nds::PVVariableIn<std::int32_t>("T4period-RB")),
    T5period(nds::PVVariableIn<std::int32_t>("T5period-RB")),
    T6period(nds::PVVariableIn<std::int32_t>("T6period-RB")),
    T7period(nds::PVVariableIn<std::int32_t>("T7period-RB")),
    periodicDisplayFrequency(nds::PVVariableIn<std::int32_t>("dispFreq-RB"))
//WARNING NO COMMA IN THE PREVIOUS LAST LINE
{
    parentNode.addChild(m_node);

    readNonPeriodicCB.mutexRetrieveData = PTHREAD_MUTEX_INITIALIZER;
    readNonPeriodicCB.condRetrieveData = PTHREAD_COND_INITIALIZER;
    readNonPeriodicCB.retieveData = false;
    for(size_t i(0); i < CB_CHANNEL_NB_MAX; ++i)
        bufsizes[i] = 0;

    // AM Channel 0 to 5 =>  6 ADC channels
    printf("\nStarting %d Algorithm Module channels for IFC1410 in slot %d\n\n", AM_CHANNEL_NB_MAX, m_deviceUser.card);
    for(size_t AM_Channel(0); AM_Channel < AM_CHANNEL_NB_MAX; ++AM_Channel)
    {
        std::ostringstream channelName;
        channelName << "CH" << AM_Channel;
        m_AIChannels.push_back(std::make_shared<IFC14AIChannel>(channelName.str(), m_node, AM_Channel, m_deviceUser));
    }

    // Management of CB_CHANNEL_NB_MAX Circular Buffer channels
    // CB Channel 0 to 13
    for(size_t AM_Channel(0); AM_Channel < CB_CHANNEL_NB_MAX; ++AM_Channel)
        processors.push_back(std::make_shared<DataProcessor>(AM_Channel, m_AIChannels));
    
    clr_exit_processData_loop();
    clr_fillFiles();

    m_testCounterPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_testCounterPV);

    collectOn.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(collectOn);

    // PV for info versions
    m_nBLM_infoVersionsPV.setScanType(nds::scanType_t::interrupt);
    m_nBLM_infoVersionsPV.setMaxElements(512);
    m_node.addChild(m_nBLM_infoVersionsPV);

    // READBACK OF PVs WHICH ARE ALSO DEFINED IN OUTPUT
    channel_mask.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(channel_mask);
    decimStart.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(decimStart);
    decimStop.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(decimStop);
    T1period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T1period);
    T2period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T2period);
    T3period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T3period);
    T4period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T4period);
    T5period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T5period);
    T6period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T6period);
    T7period.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(T7period);
    periodicDisplayFrequency.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(periodicDisplayFrequency);

    // OUTPUT PVs
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("set_CBs", std::bind(&IFC14AIChannelGroup::set_CBs, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB0", std::bind(&IFC14AIChannelGroup::set_enableCB0, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB1", std::bind(&IFC14AIChannelGroup::set_enableCB1, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB2", std::bind(&IFC14AIChannelGroup::set_enableCB2, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB3", std::bind(&IFC14AIChannelGroup::set_enableCB3, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB4", std::bind(&IFC14AIChannelGroup::set_enableCB4, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB5", std::bind(&IFC14AIChannelGroup::set_enableCB5, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB6", std::bind(&IFC14AIChannelGroup::set_enableCB6, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB7", std::bind(&IFC14AIChannelGroup::set_enableCB7, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB8", std::bind(&IFC14AIChannelGroup::set_enableCB8, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB9", std::bind(&IFC14AIChannelGroup::set_enableCB9, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB10", std::bind(&IFC14AIChannelGroup::set_enableCB10, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB11", std::bind(&IFC14AIChannelGroup::set_enableCB11, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB12", std::bind(&IFC14AIChannelGroup::set_enableCB12, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("enableCB13", std::bind(&IFC14AIChannelGroup::set_enableCB13, this, std::placeholders::_1, std::placeholders::_2)));

    m_node.addChild(nds::PVDelegateOut<std::int32_t>("configureCBs", std::bind(&IFC14AIChannelGroup::set_configureCBs, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("startDODreq", std::bind(&IFC14AIChannelGroup::set_startDODreq, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("stopDODreq", std::bind(&IFC14AIChannelGroup::set_stopDODreq, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("DODpreview", std::bind(&IFC14AIChannelGroup::set_DODpreview, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("decimStart", std::bind(&IFC14AIChannelGroup::set_decimStart, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<double>("decimStop", std::bind(&IFC14AIChannelGroup::set_decimStop, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T1period", std::bind(&IFC14AIChannelGroup::set_T1, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T2period", std::bind(&IFC14AIChannelGroup::set_T2, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T3period", std::bind(&IFC14AIChannelGroup::set_T3, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T4period", std::bind(&IFC14AIChannelGroup::set_T4, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T5period", std::bind(&IFC14AIChannelGroup::set_T5, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T6period", std::bind(&IFC14AIChannelGroup::set_T6, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("T7period", std::bind(&IFC14AIChannelGroup::set_T7, this, std::placeholders::_1, std::placeholders::_2)));
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("dispFreq", std::bind(&IFC14AIChannelGroup::set_periodicDisplayFrequency, this, std::placeholders::_1, std::placeholders::_2)));
}

void IFC14AIChannelGroup::set_configureCBs(const timespec &timespec, const int32_t &value){
    if(value != 0)
        reconfigure_CB_channels();
}

void IFC14AIChannelGroup::set_startDODreq(const timespec &timespec, const int32_t &value){
    if(value)
    {
        pthread_mutex_lock (&readNonPeriodicCB.mutexRetrieveData);
        if(readNonPeriodicCB.retieveData == false)
        {
            // Enable data collecting in files
            set_fillFiles();
            for(size_t AM_Channel(0); AM_Channel < AM_CHANNEL_NB_MAX; ++AM_Channel)
            {
                m_AIChannels.at(AM_Channel)->sampleEventIdx = 0;
                m_AIChannels.at(AM_Channel)->sampleNeutronIdx = 0;
                m_AIChannels.at(AM_Channel)->sampleRawDataIdx = 0;
            }
            // The circular buffer recording is stopped, start the on demand data collecting
            readNonPeriodicCB.retieveData = true;
            pthread_cond_broadcast (&readNonPeriodicCB.condRetrieveData);
        }
        for(size_t AM_Channel(0); AM_Channel < AM_CHANNEL_NB_MAX; ++AM_Channel)
        {
            pthread_mutex_lock (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.mutexRetrieveData);
            m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.retievePause = false;
            pthread_cond_broadcast (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.condRetrieveData);
            pthread_mutex_unlock (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.mutexRetrieveData);
        }

        pthread_mutex_unlock (&readNonPeriodicCB.mutexRetrieveData);
    }
}


void IFC14AIChannelGroup::set_stopDODreq(const timespec &timespec, const int32_t &value){
    if(value)
    {
        printf("User breaks the DOD request\n");
        // Break the data collecting
        clr_fillFiles();

        for(size_t AM_Channel(0); AM_Channel < AM_CHANNEL_NB_MAX; ++AM_Channel)
        {
            pthread_mutex_lock (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.mutexRetrieveData);
            m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.retievePause = false;
            pthread_cond_broadcast (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.condRetrieveData);
            pthread_mutex_unlock (&m_AIChannels.at(AM_Channel)->readNonPeriodicCBchannel.mutexRetrieveData);
        }
    }
} 


void IFC14AIChannelGroup::set_DODpreview(const timespec &timespec, const int32_t &value){
    if(value == 0)
        set_previewAndNoHDF5();
    else
        clr_previewAndNoHDF5();
}


void IFC14AIChannelGroup::set_decimStart(const timespec &timespec, const double &value){
    //delay in 125MHz clock cycles (= 8 ns)
    write_reg(REG_DECIMATOR_START,  (uint32_t)(value/8), m_deviceUser);
    get_decimStart();
}


void IFC14AIChannelGroup::set_decimStop(const timespec &timespec, const double &value){
    //delay in 125MHz clock cycles (= 8 ns)
    write_reg(REG_DECIMATOR_STOP,  (uint32_t)(value/8), m_deviceUser);
    get_decimStop();
}


void IFC14AIChannelGroup::set_T1(const timespec &timespec, const int32_t &value){
    lossPeriod[0] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[0] = 0;
    exitNeutronCounter(0);
    get_T1();
}

void IFC14AIChannelGroup::set_T2(const timespec &timespec, const int32_t &value){
    lossPeriod[1] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[1] = 0;
    exitNeutronCounter(1);
    get_T2();
}

void IFC14AIChannelGroup::set_T3(const timespec &timespec, const int32_t &value){
    lossPeriod[2] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[2] = 0;
    exitNeutronCounter(2);
    get_T3();
}

void IFC14AIChannelGroup::set_T4(const timespec &timespec, const int32_t &value){
    lossPeriod[3] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[3] = 0;
    exitNeutronCounter(3);
    get_T4();
}

void IFC14AIChannelGroup::set_T5(const timespec &timespec, const int32_t &value){
    lossPeriod[4] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[4] = 0;
    exitNeutronCounter(4);
    get_T5();
}

void IFC14AIChannelGroup::set_T6(const timespec &timespec, const int32_t &value){
    lossPeriod[5] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[5] = 0;
    exitNeutronCounter(5);
    get_T6();
}

void IFC14AIChannelGroup::set_T7(const timespec &timespec, const int32_t &value){
    lossPeriod[6] = (uint16_t)value;
    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[6] = 0;
    exitNeutronCounter(6);
    get_T7();
}


void IFC14AIChannelGroup::set_periodicDisplayFrequency(const timespec &timespec, const int32_t &value){
   struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    displayDiscrimination = (uint8_t)value;
    set_displayDiscrimination();
    
    // update Readback value
    periodicDisplayFrequency.push(now, (int32_t)displayDiscrimination);
}


void IFC14AIChannelGroup::reconfigure_CB_channels(void){  
    // Stop all threads (if they exist)
    set_exit_processData_loop();
     
    if(firstReconf == false)
    {
        m_mainThread.join();
        printf("RECONFIGURATION, all tasks have joined\n");
    }
    firstReconf = false;
    sleep(3);
    clr_exit_processData_loop();
    m_mainThread = m_node.runInThread("MainThread", std::bind(&IFC14AIChannelGroup::main_thread, this));
}

void IFC14AIChannelGroup::set_CBs(const timespec &timespec, const int32_t &value){
    switch(value)
    {
            case 0: //"RESET"
                resetChannels = true;
                // Stop all threads (if they exist)
                set_exit_processData_loop();
     
                if(firstReconf == false)
                {
                    m_mainThread.join();
                    printf("RECONFIGURATION, all tasks have joined\n");
                }
                firstReconf = true;
                sleep(3);
                clr_exit_processData_loop();
                channelMask = 0;

                printf("Stop acquisition and disable all CBs of the board in slot %d\n", m_deviceUser.card );
            break;

            case 1: //"ON"
                reconfigure_CB_channels();
            break;

            default:
            break;
    }
}

void IFC14AIChannelGroup::set_enableCB0(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=0;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB1(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=1;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB2(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=2;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB3(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=3;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB4(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=4;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB5(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=5;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB6(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=6;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB7(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=7;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB8(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=8;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB9(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=9;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB10(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=10;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB11(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=11;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB12(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=12;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}

void IFC14AIChannelGroup::set_enableCB13(const timespec &timespec, const int32_t &value){
    struct  timespec now;
    const uint8_t bitNb=13;

    clock_gettime(CLOCK_REALTIME, &now);

    if(value != 0)
        channelMask |= (1 << bitNb);
    else
        channelMask &= ~(uint32_t)(1 << bitNb);

    channel_mask.push(now, (int32_t)channelMask); // push new read-back value
}


void IFC14AIChannelGroup::get_decimStart(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_reg(REG_DECIMATOR_START, m_deviceUser);
     decimStart.push(now, (double)(value)*8);
}

void IFC14AIChannelGroup::get_decimStop(void){
    struct  timespec now;
    uint32_t value;

    clock_gettime(CLOCK_REALTIME, &now);
    
     value = read_reg(REG_DECIMATOR_STOP, m_deviceUser);
     decimStop.push(now, (double)(value)*8);
}

void IFC14AIChannelGroup::get_T1(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T1period.push(now, (int32_t)(lossPeriod[0]));
}

void IFC14AIChannelGroup::get_T2(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T2period.push(now, (int32_t)(lossPeriod[1]));
}

void IFC14AIChannelGroup::get_T3(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T3period.push(now, (int32_t)(lossPeriod[2]));
}

void IFC14AIChannelGroup::get_T4(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T4period.push(now, (int32_t)(lossPeriod[3]));
}

void IFC14AIChannelGroup::get_T5(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T5period.push(now, (int32_t)(lossPeriod[4]));
}

void IFC14AIChannelGroup::get_T6(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T6period.push(now, (int32_t)(lossPeriod[5]));
}

void IFC14AIChannelGroup::get_T7(void){
    struct  timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    T7period.push(now, (int32_t)(lossPeriod[6]));    
}


uint32_t IFC14AIChannelGroup::get_channel_mask(void){
    struct  timespec now;
    int32_t value;

    clock_gettime(CLOCK_REALTIME, &now);

    value = (int32_t)read_reg(REG_CB_DATA_CHANNEL_ENABLE, m_deviceUser);
    channel_mask.push(now, value);

    return (uint32_t)value;
}


void IFC14AIChannelGroup::getnBLM_versions()
{
    std::string value;
    char bitstream_date[20] = { 0 };
    char epicsApp_date[20] = { 0 };
    char epicsDisplay[512] = { 0 };
    int i=0;
    struct timespec now;
    
    clock_gettime(CLOCK_REALTIME, &now);

    strcat(epicsApp_date, __DATE__);
    strcat(epicsApp_date, " ");
    strcat(epicsApp_date, __TIME__); 

    read_versions(bitstream_date, m_deviceUser);

    i+=sprintf(epicsDisplay+i, "  nblmapp EPICS version: %s - Built time: %s\n", EPICS_APP_VERSION, epicsApp_date);
	sprintf(epicsDisplay+i,    "  nBLM firmware built time: %s", bitstream_date);

    // Startup display
    printf(  "\n____________________________________________________________________\n\n");
    printf("%s", epicsDisplay);
    printf("\n\n____________________________________________________________________\n\n");
    
    // EPICS Readback
    value = std::string(epicsDisplay);
    m_nBLM_infoVersionsPV.push(now, value);
}


bool IFC14AIChannelGroup::allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal)
{
    return true;
}


void IFC14AIChannelGroup::processPeriodicData (void)
{
    printf("Starting processPeriodicData() task\n");
    while((!exit_loop) && (!exit_processData_loop) )
    {
        // wait periodic data : timeout = 20 ms
        processors.at(cb_channel_periodic)->processOnlineData(20);
    }
    printf("End of processPeriodicData() task\n");
}


void IFC14AIChannelGroup::processNonPeriodicData (void)
{
  struct timespec timeout, now;
  int result;
  uint32_t chan_mask = channelMask;

    printf("Starting processNonPeriodicData() task\n");
    while((!exit_loop) && (!exit_processData_loop) )
    {
      pthread_mutex_lock (&readNonPeriodicCB.mutexRetrieveData);
      clock_gettime(CLOCK_REALTIME, &timeout);
      // timeout = 1 second
      timeout.tv_sec += 1;
      result = 0;
      // Wait with Timeout for a signal to retrieve the on demand data (then CSS monitoring or HDF5 storage)
      while ((!readNonPeriodicCB.retieveData) && (!exit_loop) && (!exit_processData_loop))
      {
        result = pthread_cond_timedwait (&readNonPeriodicCB.condRetrieveData, &readNonPeriodicCB.mutexRetrieveData, &timeout);
        if (result == ETIMEDOUT)
        {
            clock_gettime(CLOCK_REALTIME, &timeout);
            timeout.tv_sec += 1;
        }        
      }
      pthread_mutex_unlock (&readNonPeriodicCB.mutexRetrieveData);

      if(!exit_loop && !exit_processData_loop)
      {
        clock_gettime(CLOCK_REALTIME, &now);
        collectOn.push(now, (int32_t)true);
        sleep(1);

        // CSS Preview or HDF5 writing
        if(DODpreview)
        {
            m_consumerThread.clear(); // only non periodic CB
            // Run the non periodic CB processing tasks for activated CB channels
            for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ch++)
                if (chan_mask & (~(uint32_t)(1 << cb_channel_periodic)) & (1 << ch))
                {
                    processors.at(ch)->enableHDF5 = false;
                    printf("Run thread processOfflineDataInterleaved for CB%d\n", ch);
                    // Now we will start to process data in the "big" CPU DDR area for the CSS preview only (no HDF5 files writing)
                    m_consumerThread.push_back(m_node.runInThread(std::bind(&IFC14AIChannelGroup::processDOD, this, ch, 0)));
                }

            for (uint8_t i = 0; i < m_consumerThread.size(); ++i)
            {
                m_consumerThread.at(i).join();
                printf("Close thread processOfflineDataInterleaved for the non periodic channels\n");
            }
        }
#ifdef USE_HDF5
        else
        {
            for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
                m_AIChannels.at(m_AM_Channel)->hdf5Obj->init_one_HDF5();

            // Run sequencialy the non periodic CB processing function for activated CB channels
            for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ch++)
                if (chan_mask & (~(uint32_t)(1 << cb_channel_periodic)) & (1 << ch))
                {
                    processors.at(ch)->enableHDF5 = true;
                    processDOD(ch, 0);
                }

            for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
                m_AIChannels.at(m_AM_Channel)->hdf5Obj->flush_one_HDF5();
        }
#endif

        for (int ch = 0; ch < CB_CHANNEL_NB_MAX; ch++)
            if (chan_mask & (~(uint32_t)(1 << cb_channel_periodic)) & (1 << ch))
            {
                processors.at(ch)->buffers.flush();
                processors.at(ch)->buffers_timestamped.flush();
            }

        // All data have been retrieved or DOD request has been break
        clr_fillFiles(); // In case of all data have been retrieved (not a DOD break)

        // Set to false the data collecting when all data have been retrieved. The circular buffer recording is restarted
        pthread_mutex_lock (&readNonPeriodicCB.mutexRetrieveData);
        if(readNonPeriodicCB.retieveData)
        {
            readNonPeriodicCB.retieveData = false;
            pthread_cond_broadcast (&readNonPeriodicCB.condRetrieveData);
        }
        pthread_mutex_unlock (&readNonPeriodicCB.mutexRetrieveData);
            
        clock_gettime(CLOCK_REALTIME, &now);
        collectOn.push(now, (int32_t)false);
      }
    }
    printf("End of processNonPeriodicData() task\n");
}


template<class T>
T duree(const uint8_t period, T t, T u, T v, T w, T x, T y, T z)
{   switch(period)
    {
        case 0:
        return t;
        case 1:
        return u;
        case 2:
        return v;
        case 3:
        return w;
        case 4:
        return x;
        case 5:
        return y;
        case 6:
        return z;
        default:
        printf("error in duree function\n");
        return u;
    }
}

void IFC14AIChannelGroup::accumulatedLossThread(const uint8_t T)
{
    struct timespec now;
    using days        = std::chrono::duration<double, std::ratio<86400, 1>>;
    using weeks       = std::chrono::duration<double, std::ratio<604800, 1>>;
    using months      = std::chrono::duration<double, std::ratio<2629746, 1>>;
    using hours       = std::chrono::duration<double, std::ratio<3600, 1>>;
    using minutes     = std::chrono::duration<double, std::ratio<60, 1>>;
    using seconds     = std::chrono::duration<double, std::ratio<1, 1>>;
    using milliseconds= std::chrono::duration<double, std::ratio<1, 1000>>;
    using Trp         = std::chrono::duration<double, std::ratio<1, 14>>;
    uint16_t period;

	printf("Starting accumulatedLossThread(%d) task\n", T);
    while( (!exit_loop) && (!exit_processData_loop) )
    {
        // For less CPU activity, set the maximum period when lossPeriod[T] == 0
        if(lossPeriod[T] == 0)
            period = 65535;
        else
            period = lossPeriod[T];
        
        auto dur0 = std::chrono::duration_cast<milliseconds>(Trp(period));
        auto dur1 = std::chrono::duration_cast<milliseconds>(months(period));
        auto dur2 = std::chrono::duration_cast<milliseconds>(weeks(period));
        auto dur3 = std::chrono::duration_cast<milliseconds>(days(period));
        auto dur4 = std::chrono::duration_cast<milliseconds>(hours(period));
        auto dur5 = std::chrono::duration_cast<milliseconds>(minutes(period));
        auto dur6 = std::chrono::duration_cast<milliseconds>(seconds(period));

        auto dur = duree(T, dur0, dur1, dur2, dur3, dur4, dur5, dur6);
 
        std::unique_lock<std::mutex> lck(mtxCounter[T]);
        {
            // Sleep this thread for "dur" milliseconds (but stop sleeping when exitSleep[T]== true)
            if( cv[T].wait_for(lck, dur, [this, T](){return exitSleep[T];}) )
            {
                exitSleep[T] = false;
            }
            else // timeout
            {
                if(lossPeriod[T] != 0)
                    for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
                    {
                        clock_gettime(CLOCK_REALTIME, &now);
                        m_AIChannels.at(m_AM_Channel)->lossOverT.at(T).push(now, m_AIChannels.at(m_AM_Channel)->accumulatedLosses[T]);
                        m_AIChannels.at(m_AM_Channel)->accumulatedLosses[T] = 0;
                    }
            }
        }
    }
    printf("End of accumulatedLossThread(%d) task\n", T);
}


long elapsed_time(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return (temp.tv_nsec+temp.tv_sec*1000000000);
}    

extern "C" {
  epicsExportAddress(int, onAMCOne);
}
