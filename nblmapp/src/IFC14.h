/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by the European Spallation Source ERIC
 * Author: Joao Paulo Martins 
 * 		   ESS, Lund, Sweden
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifndef IFC14_H
#define IFC14_H


#include <iostream>
#include <sstream>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * @param text Has to be something that can be passed to std::string()
 */

#define IFC14NDS_MSGWRN(text)                         \
    do {                                               \
        struct timespec now;                           \
        clock_gettime(CLOCK_REALTIME, &now);           \
        m_infoPV.push(now, std::string(text));                    \
        ndsWarningStream(m_node) << std::string(text) << "\n";    \
    } while(0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define IFC14NDS_STATUS_MSGERR(func, status)                      \
    do {                                                           \
        std::ostringstream tmp;                                    \
        tmp << (func) << " " <<                                    \
                    ifcdaqdrv_strerror(status) << (status);        \
        IFC14NDS_MSGWRN(tmp.str());                               \
    } while(0)

#define IFC14NDS_STATUS_CHECK(func, status)                                \
    do {                                                                    \
        if ((status) != status_success) {                                   \
            IFC14NDS_STATUS_MSGERR(func, status);                          \
            /*throw nds::NdsError("ifcdaqdrv returned error");*/            \
        }                                                                   \
    } while (0)


#endif /* IFC14_H */
