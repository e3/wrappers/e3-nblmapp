/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifndef __HDF5_INTERFACE_H__
#define __HDF5_INTERFACE_H__

#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include "nblmdrv.h"

//#define USE_NAPI
#ifdef USE_NAPI
#include "napi.h"
#else
#include "hdf5.h"
#endif

struct hdf5_timestampAndSample
{
  uint64_t evt_timestamp;
  uint64_t timestamp;
  uint16_t sample;
};

struct hdf5_neutronCount
{
  uint64_t evt_timestamp;
  uint32_t MTWindx;
  int32_t q_N;
  int32_t background_count;
  uint8_t n;
  uint8_t negative_saturations;
  uint8_t positive_saturations;
  uint8_t channel;
};

struct hdf5_eventInfo
{
  uint64_t evt_timestamp;
  uint32_t MTWindx;
  int32_t Q_TOT;
  int32_t peakValue;
  uint16_t TOT;
  uint16_t TOTstartTime;
  uint16_t peakTime;
  uint16_t serialNumber;
  uint8_t flags;
  uint8_t channel;
};

struct hdf5_triggerEventInfo
{
  uint64_t int_timestamp;
  uint64_t evt_timestamp;
  uint8_t channel;
};


class hdf5_for_one_ifc
{
public:
  //hdf5_for_one_ifc(ifcdaqdrv_usr_t &deviceUser, int32_t channelNum);
  hdf5_for_one_ifc(ifcdaqdrv_usr_t &deviceUser, int32_t channelNum) : m_deviceUser(deviceUser), m_AM_Channel(channelNum) {};

  template <class T>  void append_HDF5_data(std::vector<T>& b, const char *datasetPathAndName, unsigned long long & offsetWhereToWrite, hid_t& type);
  void init_one_HDF5(void);
  void flush_one_HDF5(void);
  void writeTimestampAndSampleToHdf (const hdf5_timestampAndSample * data);
  void writeNeutronCountToHdf (const hdf5_neutronCount & data);
  void writeEventInfoToHdf (const hdf5_eventInfo & data);
  void writeTriggerEventInfoToHdf (const hdf5_triggerEventInfo & data);

private:
  ifcdaqdrv_usr_t &m_deviceUser;
  std::int32_t     m_AM_Channel; //Algorithm Module channel
  std::string      HDF_FILE;

  long unsigned int     index_chunk_dataset;
  unsigned long long    timestampAndSample_dataset_size;
  unsigned long long    neutronCount_dataset_size;
  unsigned long long    eventInfo_dataset_size;
  unsigned long long    triggerEventInfo_dataset_size;
  unsigned long long    lastTimestampAndSampleIndex;
  unsigned long long    lastNeutronCountIndex;
  unsigned long long    lastEventInfoIndex;
  unsigned long long    lastTriggerEventInfoIndex;
  std::vector<hdf5_timestampAndSample>  timestampAndSample_buffer;
  std::vector<hdf5_neutronCount>        neutronCount_buffer;
  std::vector<hdf5_eventInfo>           eventInfo_buffer;
  std::vector<hdf5_triggerEventInfo>    triggerEventInfo_buffer;

  // Dataset identifiers
  hid_t hdf5_timestampAndSample_id;
  hid_t hdf5_neutronCount_id;
  hid_t hdf5_eventInfo_id;
  hid_t hdf5_triggerEventInfo_id;
  hid_t dataspace_id;
};

#endif /* __HDF5_INTERFACE_H__ */
