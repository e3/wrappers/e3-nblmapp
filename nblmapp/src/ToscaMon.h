/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#ifndef _TOSCA_MON_H_
#define _TOSCA_MON_H_

typedef int ComFunction ();

/* A structure which contains information on the commands this program can understand. */
typedef struct {
	char		*name;		/* User printable name of the function. */
	ComFunction	*func;		/* Function to call to do the job. */
	char		**doc;		/* Documentation for this function.  */
	char		*summary;	/* Short summary for this function.  */
} COMMAND;


void history_show(int howMany, char *filter);

extern int isInteractive();
extern int getNextInputChar();

extern COMMAND commands[];
extern int programDone;
extern int revertInput(int andexit);
extern int execute_cmd(char *line);

#endif
