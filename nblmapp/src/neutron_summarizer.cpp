/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#include <stdint.h>
#include <math.h>
#include "evdet.h"

#include <iostream>
using namespace std;



void
neutron_summarizer (hls::stream<neutronCounts>& N, hls::stream<neutronCounts>& E, hls::stream<neutronCounts>& E2, hls::stream<neutronCounts>& E3, hls::stream<neutronCounts>& E4, hls::stream<neutronCounts>& E5)
{
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY min=1 max=1
#pragma HLS INTERFACE axis port=E

#pragma HLS INTERFACE axis port=E2
#pragma HLS DATA_PACK variable=E2

#pragma HLS INTERFACE axis port=E3
#pragma HLS DATA_PACK variable=E3

#pragma HLS INTERFACE axis port=E4
#pragma HLS DATA_PACK variable=E4

#pragma HLS INTERFACE axis off port=E5
#pragma HLS DATA_PACK variable=E5

#pragma HLS INTERFACE axis off port=N
#pragma HLS DATA_PACK variable=N
#pragma HLS INTERFACE ap_ctrl_none port=return

static neutronCounts sum;

static bool was_pulseTrigger; // so we do not miss pulse trigger


if (N.empty()) return;

neutronCounts nc;
N >> nc;

if(nc.dataValid)
{
	sum.N_n += nc.N_n;
	sum.N_qtot += nc.N_qtot;
        sum.Q_background += nc.Q_background;
}


if (nc.triggers.pulseTrigger)
  was_pulseTrigger = true;

sum.dataValid = false; //unused
sum.newFrame = nc.newFrame;
sum.triggers = nc.triggers;
sum.positive_saturations = nc.positive_saturations;
sum.negative_saturations = nc.negative_saturations;
sum.Qtotal = nc.Qtotal;

//E << sum;

if(nc.newFrame)
{
  if (was_pulseTrigger)
    sum.triggers.pulseTrigger = true;

  was_pulseTrigger = false;

  E << sum;
  E2 << sum;
  E3 << sum;
  E4 << sum;
  E5 << sum;

//if(nc.newFrame)
//{
#if 0
#ifndef DATA_RECEIVER 
#ifndef  __SYNTHESIS__ 
        cout << "Neutron summary:" << (uint32_t)sum.N_n << " - " << sum.N_qtot << " - " << (int32_t)sum.Q_background * (1.0/65535) << endl;
#endif
#endif
#endif
	sum.N_n = 0;
	sum.N_qtot = 0.0f;
	sum.Q_background = 0;
}

}
