import epics    # caget caput
import sys      # shell args

sys.path.insert(0, "/opt/epics/modules/nblmapp/1.0.0/misc")
from epicsFunctions import * # secure caget and caput (blocant function with read back)

# macros default value
PREFIX_DEFAULT  = "IFC1410_nBLM"
DEVICE_TUNING   = "Ctrl-AMC-130"      
CHANNEL_DEFAULT = "CH0"              

# get macros given in argv
nbOfArgRequired = 4
if len(sys.argv) == nbOfArgRequired :
    # macros
    PREFIX          = sys.argv[1]
    DEVICE_TUNING   = sys.argv[2]
    CHANNEL         = sys.argv[3] # "CHx"
else:
    print "[W] Wrong number of arguments given:", len(sys.argv), ". Should be", nbOfArgRequired
    print "[W] use defaut value"
    PREFIX          = PREFIX_DEFAULT
    DEVICE_TUNING   = DEVICE_TUNING
    CHANNEL         = CHANNEL_DEFAULT


pvs = [ connectPV(PREFIX + ":" + DEVICE_TUNING + ":" + CHANNEL + ":histo" + "ChargeMPV"    + "clear"),
        connectPV(PREFIX + ":" + DEVICE_TUNING + ":" + CHANNEL + ":histo" + "TOTMPV"       + "clear"),
        connectPV(PREFIX + ":" + DEVICE_TUNING + ":" + CHANNEL + ":histo" + "PEDESTAL"     + "clear"),
        connectPV(PREFIX + ":" + DEVICE_TUNING + ":" + CHANNEL + ":histo" + "RiseTimeMPV"  + "clear"),
        connectPV(PREFIX + ":" + DEVICE_TUNING + ":" + CHANNEL + ":histo" + "AmplitudeMPV" + "clear")
]

# clear histo
for i in range(len(pvs)):
    myCaput (pvs[i], 1, pvs[i])
    print "Clear done:", pvs[i]

# falling edge is done by PV (bo, HIGH field)
# print myCaget(pvs[i]), pvs[i]
# time.sleep(2)
# print myCaget(pvs[i]), pvs[i]
