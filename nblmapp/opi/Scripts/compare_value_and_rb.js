importPackage(Packages.org.csstudio.opibuilder.scriptUtil);


var value = PVUtil.getDouble(pvs[0]) // value: setter (IOC). This value is send to FPGA.
var value_rb = PVUtil.getDouble(pvs[1]) // read-back value of the setter (the actual value on the FPGA)

if (value != value_rb){
	widget.setPropertyValue("border_color","Major");
	// ConsoleUtil.writeInfo("error RB");
}
else {
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,255,0));
	// ConsoleUtil.writeInfo("ok");
}

