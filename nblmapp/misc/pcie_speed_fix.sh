#!/bin/sh
# lspci -tv to see PCIE_ADDR (IOxOS Technologies SA PCIeEP 1 IFC14XX)
PCIE_ADDR="4:00.0"
echo "Detecting link speed AMC 5..."
if sudo setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
        echo "PCIe Gen 3 detected! No action needed."
else
        if sudo setpci -s $PCIE_ADDR 78.l | grep -q '42'; then
                echo "PCIe Gen 2 detected! No action needed."
        else
                sudo setpci -s $PCIE_ADDR 78.l=20
                if setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
                        echo "PCIe Gen 3 detected! Done."
                else
                        echo "Something went wrong...no board detected!"
                fi
        fi
fi
PCIE_ADDR="4:02.0"
echo "Detecting link speed AMC 3..."
if sudo setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
        echo "PCIe Gen 3 detected! No action needed."
else
        if sudo setpci -s $PCIE_ADDR 78.l | grep -q '42'; then
                echo "PCIe Gen 2 detected! No action needed."
        else
                sudo setpci -s $PCIE_ADDR 78.l=20
                if setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
                        echo "PCIe Gen 3 detected! Done."
                else
                        echo "Something went wrong...no board detected!"
                fi
        fi
fi  
