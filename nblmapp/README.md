## nblmapp

This library exposes an interface to IOXOS mTCA DAQ boards with a specific firmware dedicated to the nBLM.
It also provides an EPICS interface thanks to NDS3.

